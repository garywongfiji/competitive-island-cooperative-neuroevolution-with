/*
 * myGaPcx.cpp
 *
 *  Created on: 11/08/2009
 *      Author: rohit
 */
#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <ctime>


#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<ctype.h>

time_t TicTime;
time_t TocTime;
using namespace::std;

typedef vector<double> Layer;
typedef vector<double> Nodes;
typedef vector<double> Frame;
typedef vector<int> Sizes;
typedef vector<vector<double> > Weight;
typedef vector<vector<double> > Data;

 const int LayersNumber = 3; //total number of layers.

 //const int MaxVirtLayerSize = 105;

 const int culturalPopSize = 20;
  const int CCPOPSIZE = 200;
 
 const int depthinc = 5;
 const int LastDep = 30;

//-----------------

      
     /*const double mintrain = 100;
     const int maxgen =2000;
     const int trainsize = 4  ;//110 // 138
     const int testsize  =  4 ;//40
     const   int input =  2; //13-4
     const int output  = 1; //3-2
     char *  trainfile = "xor.txt";
     char  * testfile= "xor.txt"; 
    const int fixedhidden = 2;*/
      
    /*const double mintrain = 100;
     const int maxgen =30000;
     const int trainsize = 16  ;//110 // 138
     const int testsize  = 16 ;//40
     const   int input =  4; //13-4
     const int output  = 1; //3-2
     char *  trainfile = "4bit.txt";
     char  * testfile= "4bit.txt"; */
    
      
 
       const int maxgen =15000; 
 const int fixedhidden = 5;

const double mintrain = 95;
      
     const int trainsize = 110 ;//110 // 138
     const int testsize  = 40 ;//40
     const   int input =  4; //13-4
     const int output  = 2;  
      char *  trainfile = "iris.csv";
      char  * testfile= "test.csv"; 

/*
      const double mintrain = 98;
     const int maxgen =30000;
     const int trainsize = 110  ;//110 // 138
     const int testsize  =  40 ;//40
     const   int input =  4; //13-4
     const int output  = 2; //3-2
     char *  trainfile = "iris.csv";
     char  * testfile= "test.csv"; */

//-----------------------
const double alpha = 0.00001;
const double MaxErrorTollerance = 0.20;
const double MomentumRate = 0;
const double Beta = 0;
int row ;
int col;
int layer;
int r;
int x;
int y;

double weightdecay = 0.005;

 //#include "objective.h"    //objective function
  //#include "random.h"       //random number generator
 #include "RandomNumber.h"       //random number generator


#define rosen          // choose the function:
//#define onelevel   
#define neuronlevel   //hiddenNeuron, weightlevel, neuronlevel
//fl#define r_neuronlevel

#define EPSILON 1e-50

#define MAXFUN 50000000  //upper bound for number of function evaluations

#define MINIMIZE 1      //set 1 to minimize and -1 to maximize
#define LIMIT 1e-20     //accuracy of best solution fitness desired
#define KIDS 2          //pool size of kids to be formed (use 2,3 or 4)
#define M 1             //M+2 is the number of parents participating in xover (use 1)
#define family 2        //number of parents to be replaced by good individuals(use 1 or 2)
#define sigma_zeta 0.1
#define sigma_eta 0.1   //variances used in PCX (best if fixed at these values)

//const int  PopSize = 100;

#define  NoSpeciesCons 300
//#define  NumV 3


#define NPSize KIDS + 2   //new pop size

#define RandParent M+2     //number of parents participating in PCX


#define MAXRUN 10      //number of runs each with different random initial population

double d_not[CCPOPSIZE];

double seed,basic_seed;



int RUN;




class TrainingExamples{
       friend class NeuralNetwork;
    protected:
       Data  InputValues;
       Data  DataSet;
       Data  OutputValues;
	   char* FileName;
	   int Datapoints;
       int colSize ;
       int inputcolumnSize ;
       int outputcolumnSize ;
    int datafilesize  ;
    public:
    TrainingExamples()
    {

    } ;

    TrainingExamples( char* File, int size, int length, int inputsize, int outputsize ){

       inputcolumnSize = inputsize ;
       outputcolumnSize = outputsize;
   datafilesize = inputsize+ outputsize;
      colSize = length;
      Datapoints= size;

      FileName = File;
      InitialiseData();
        }

   void printData();

   void InitialiseData();

};
 //.................................................
 void TrainingExamples:: InitialiseData()
 {
 	ifstream in( FileName );
    if(!in) {
    cout << endl << "failed to open file" << endl;

    }
    //initialise input vectors
   for(int r=0; r <  Datapoints ; r++)
   DataSet.push_back(vector<double> ());

   for(int row = 0; row < Datapoints ; row++) {
   for(int col = 0; col < colSize ; col++)
     DataSet[row].push_back(0);
      }
     //  cout<<"printing..."<<endl;
    for(  int row  = 0; row   < Datapoints ; row++)
    for( int col  = 0; col  < colSize; col ++)
      in>>DataSet[row ][col];
      //-------------------------
    //initialise output vectors
  for(int  r=0; r < Datapoints; r++)
    InputValues.push_back(vector<double> ());

     for(int  row = 0; row < Datapoints ; row++)
     for(col = 0; col < inputcolumnSize ; col++)
      InputValues[row].push_back(0);

     for(int  row = 0; row < Datapoints ; row++)
    for(int col = 0; col < inputcolumnSize ;col++)
   InputValues[row][col] = DataSet[row ][col] ;
       //-------------------------
       //initialise output vectors
 for(int r=0; r < Datapoints; r++)
    OutputValues.push_back(vector<double> ());

    for( int row = 0; row < Datapoints ; row++)
    for( int col = 0; col < outputcolumnSize; col++)
        OutputValues[row].push_back(0);

     for( int row = 0; row < Datapoints ; row++)
     for(int  col = 0; col <  outputcolumnSize;col++)
       OutputValues[row][col]= DataSet[row ][ col +inputcolumnSize ] ;

       in.close();
 }

  void TrainingExamples:: printData()
   {
      // cout<<"printing...."<<endl;
      for(int row = 0; row < Datapoints ; row++) {
     for(int col = 0; col < colSize ; col++)
         cout<<  DataSet[row][col]<<" ";

         cout<<row<<endl;
        }
    for(int  row = 0; row < Datapoints ; row++) {
    for( int col = 0; col < inputcolumnSize; col++)
       cout<<  InputValues[row][col]<<" ";

        cout<<endl;
        }
   for( int row = 0; row < Datapoints ; row++)  {
     for( int col = 0; col <  outputcolumnSize;col++)
      cout<< OutputValues[row][col] <<" " ;
      cout<<endl;
      }
    }


//*********************************************************
class Layers{
      friend class NeuralNetwork;

      protected:
          Weight Weights ;
          Weight WeightChange;
            Weight H;
          Layer Outputlayer;
          Layer Bias;
            Layer B;
           Layer Gates;
          Layer BiasChange;
          Layer Error;

       public:
        Layers()
        {

        }

      };

//***************************************************

//typedef vector<Layers> Nlayer;
class NeuralNetwork{
          friend class GeneticAlgorithmn;
          friend class CoEvolution;
      protected:
    // Nlayer nLayer(4);

     Layers nLayer[LayersNumber];

     double Heuristic;
     int StringSize;
     Layer ChromeNeuron;
        int NumEval;
     Data Output;
           Sizes layersize;

 
  
     public:
        NeuralNetwork( )
        {
        

        }
       NeuralNetwork(Sizes layer )
     {
      layersize  = layer;
    //  sampleSize = SampleSize;
     // columnSize = ColumnSize;
     // outputSize = OutputSize;

     StringSize = (layer[0]*layer[1])+(layer[1]*layer[2]) +   (layer[1] + layer[2]);
     // NumEval =0;
     }

      void PlaceHeuristic(double H)
      {

          Heuristic = H;
           }
   double Random();

   Layer BPchrome(){return ChromeNeuron;}

   double Sigmoid(double ForwardOutput);

   void CreateNetwork(Sizes Layersize ,TrainingExamples TraineeSamples);

   double  ForwardPassP(TrainingExamples TraineeSamples,int patternNum,Sizes Layersize);
 

   void ForwardPass(TrainingExamples TraineeSamp,int patternNum,Sizes Layersize);

   void BackwardPass(TrainingExamples TraineeSamp,double LearningRate,int patternNum,Sizes Layersize);

   void PrintWeights(Sizes Layersize);// print  all weights
  //
   bool ErrorTolerance(TrainingExamples TraineeSamples,Sizes Layersize, double TrainStopPercent);

     bool ErrorToleranceHalf(TrainingExamples TraineeSamples,Sizes Layersize, double TrainStopPercent);

   double SumSquaredError(TrainingExamples TraineeSamples,Sizes Layersize);

  int BackPropogation(  TrainingExamples TraineeSamples, double LearningRate,Sizes Layersize,char* Savefile,bool load);

Layer BackpropogationCICC(Layer NeuronChrome, TrainingExamples TraineeSamples, double LearningRate,Sizes Layersize,char* Savefile,bool load, int EpochMax, ofstream &out1);
   void SaveLearnedData(Sizes Layersize,char* filename) ;

  double Rate(TrainingExamples TraineeSamples,Sizes Layersize);

   void LoadSavedData(Sizes Layersize,char* filename) ;

   double TestLearnedData(Sizes Layersize,char* filename,int  size, char* load,  int inputsize, int outputsize );

  
  double  BP(  Layer NeuronChrome,   TrainingExamples Test, int generations);

   Layer  Neurons_to_chromes( );

   double  CountLearningData(TrainingExamples TraineeSamples,int temp,Sizes Layersize);

          double  TestTrainingData(Sizes Layersize, char* filename,int  size, char* load,  int inputsize, int outputsize  );

   double CountTestingData(TrainingExamples TraineeSamples,int temp,Sizes Layersize);

 

    double LearningRate(TrainingExamples TraineeSamples,Sizes Layersize, int pattern);

bool CheckOutput(TrainingExamples TraineeSamples,int pattern,Sizes Layersize);

   double GenerateTreeData(char* TestingFile,int testsize,Sizes Layersize, char* filename,char* load)  ;

   void  ChoromesToNeurons(  Layer NeuronChrome);

double ForwardFitnessPass(  Layer NeuronChrome,   TrainingExamples Test) ;

    };
double NeuralNetwork::Random()
{     int chance;
      double randomWeight;
      double NegativeWeight;
      chance =rand()%2;

      if(chance ==0){
      randomWeight =rand()% 100;
      return randomWeight*0.005;
       }

      if(chance ==1){
      NegativeWeight =rand()% 100;
      return NegativeWeight*-0.005;
     }

}
double NeuralNetwork::Sigmoid(double ForwardOutput)
{
      double ActualOutput;
     ActualOutput = (1.0 / (1.0 + exp(-1.0 * (ForwardOutput) ) ));

    return  ActualOutput;
}

//flag

void NeuralNetwork::CreateNetwork(Sizes Layersize,TrainingExamples TraineeSamples)
{
         // for(int layer=0; layer < Layersize.size(); layer++)
         // cout<<Layersize[layer]  <<endl;

  int end = Layersize.size() - 1;

for(layer=0; layer < Layersize.size() - 1; layer++){
  //    int row = 0, int r = 0;
      //-------------------------------------------
        for(int  r=0; r < Layersize[layer]; r++)
           nLayer[layer].Weights.push_back(vector<double> ());

        for( int row = 0; row< Layersize[layer] ; row++)
          for( int col = 0; col < Layersize[layer+1]; col++)
          nLayer[layer].Weights[row].push_back(Random());
     //---------------------------------------------
        for(int  r=0; r < Layersize[layer]; r++)
          nLayer[layer].WeightChange.push_back(vector<double> ());

        for( int row = 0; row < Layersize[layer] ; row ++)
          for( col = 0; col < Layersize[layer+1]; col++)
           nLayer[layer].WeightChange[row ].push_back(0);

            for( int r=0; r < Layersize[layer]; r++)
          nLayer[layer].H.push_back(vector<double> ());

        for(int  row = 0; row < Layersize[layer] ; row ++)
          for( int col = 0; col < Layersize[layer+1]; col++)
           nLayer[layer].H[row ].push_back(0);
     //---------------------------------------------
     }

    for( layer=0; layer < Layersize.size(); layer++){
     for( row = 0; row < Layersize[layer] ; row ++)
         nLayer[layer].Outputlayer.push_back(0);

     for( row = 0; row < Layersize[layer] ; row ++)
         nLayer[layer ].Bias.push_back(Random());

       for( row = 0; row < Layersize[layer] ; row ++)
         nLayer[layer ].Gates.push_back(0);



      for( row = 0; row < Layersize[layer] ; row ++)
         nLayer[layer ].B.push_back(0);

     for( row = 0; row < Layersize[layer] ; row ++)
         nLayer[layer ].BiasChange.push_back(0);

     for( row = 0; row < Layersize[layer] ; row ++)
         nLayer[layer ].Error.push_back(0);
     }
    //--------------------------------------

    for( r=0; r < TraineeSamples.Datapoints; r++)
           Output.push_back(vector<double> ());
    for( row = 0; row< TraineeSamples.Datapoints ; row++)
          for( col = 0; col < Layersize[end]; col++)
           Output[row].push_back(0);
   for( row = 0; row < StringSize ; row ++)
         ChromeNeuron.push_back(0);

  //      SaveLearnedData(Layersize, "createnetwork.txt");

 //        LoadInductiveBias();

    //  SaveLearnedData(Layersize, "SetRules.txt");
  //SaveLearnedData(layersize,"Learnt.txt");
}
void NeuralNetwork::ForwardPass(TrainingExamples TraineeSamples,int patternNum,Sizes Layersize)
{
   double WeightedSum = 0;
      double ForwardOutput;




      int end = Layersize.size() - 1;

       for(int row = 0; row < Layersize[0] ; row ++)
        nLayer[0].Outputlayer[row] = TraineeSamples.InputValues[patternNum][row];
 


    //--------------------------------------------
   for(int layer=0; layer < Layersize.size()-1; layer++){

 for(int y = 0; y< Layersize[layer+1]; y++) {
    for(int x = 0; x< Layersize[layer] ; x++){
       	WeightedSum += (nLayer[layer].Outputlayer[x] * nLayer[layer].Weights[x][y]);

        ForwardOutput = WeightedSum - nLayer[layer+1].Bias[y];
     }
 // nLayer[layer+1].Outputlayer[y] = Sigmoid(ForwardOutput);
  nLayer[layer+1].Outputlayer[y] = Sigmoid(ForwardOutput);

 WeightedSum = 0;
 }
 WeightedSum = 0;
}//end layer

   //--------------------------------------------
   for(int output= 0; output < Layersize[end] ; output ++){
    Output[patternNum][output] = nLayer[end].Outputlayer[output];
  
}

 }

 void NeuralNetwork::BackwardPass(TrainingExamples TraineeSamp,double LearningRate,int patternNum,Sizes Layersize)
 {
    int end = Layersize.size() - 1;// know the end layer
    double temp = 0;

  // LearningRate = Rate( TraineeSamp,Layersize);
    //----------------------------------------
    // compute error gradient for output neurons
 for(int output=0; output < Layersize[end]; output++) {
   nLayer[end].Error[output] = (Output[patternNum][output]*(1-Output[patternNum][output]))*(TraineeSamp.OutputValues[patternNum][output]-Output[patternNum][output]);


    }
    //----------------------------------------
     for(int layer = Layersize.size()-2; layer != 0; layer--){

       for( x = 0; x< Layersize[layer] ; x++){  //inner layer
       for( y = 0; y< Layersize[layer+1]; y++) { //outer layer
    	temp += ( nLayer[layer+1].Error[y] * nLayer[layer].Weights[x][y]);
            }
      	nLayer[layer].Error[x] = nLayer[layer].Outputlayer[x] * (1-nLayer[layer].Outputlayer[x]) * temp;
  //	cout<<	nLayer[layer].Error[x]<<" is error of  "<<layer<<endl;
 	temp = 0.0;
     }
    temp = 0.0;

  }
    //---------------------------------------
  	double tmp;
  	int layer =0;
  for( layer = Layersize.size()-2; layer != -1; layer--){
  //      cout<<"layer"<<layer<<endl;
   for( x = 0; x< Layersize[layer] ; x++){  //inner layer
     for( y = 0; y< Layersize[layer+1]; y++) { //outer layer
      tmp = (( LearningRate * nLayer[layer+1].Error[y] * nLayer[layer].Outputlayer[x])  );// weight change
     //  - (Beta * nLayer[layer].Weights[x][y])
       	 nLayer[layer].Weights[x][y] += ( tmp  -  ( alpha * tmp) ) ;
    //	nLayer[layer].WeightChange[x][y] = tmp;
  //	cout<<nLayer[layer].Weights[x][y]<<" is updated  weight of layer "<<layer<<endl;
            }
      }
   }

   double tmp1;
   //---------------------------------------
    for( layer = Layersize.size()-1; layer != 0; layer--){
      //  cout<<"layer"<<layer<<endl;
         for( y = 0; y< Layersize[layer]; y++){
          tmp1 = (( -1 * LearningRate * nLayer[layer].Error[y])  );
          nLayer[layer].Bias[y] +=  ( tmp1 - (alpha * tmp1))  ;
        // nLayer[layer].BiasChange[y]= tmp1 ;
      //   	cout<<nLayer[layer].Bias[y]<<" is updated  Bias for layer "<<layer<<endl;
        }
   }
  //---------------------------------------

 }


bool NeuralNetwork::ErrorTolerance(TrainingExamples TraineeSamples,Sizes Layersize, double TrainStopPercent)
{
    double count = 0;
    int total = TraineeSamples.Datapoints;
    double accepted = total;
      double desiredoutput;
    double actualoutput;
    double Error;
    int end = Layersize.size() - 1;

 for(int pattern = 0; pattern< TraineeSamples.Datapoints; pattern++){



    Layer Desired;
    Layer Actual;

    for(int i = 0; i <  Layersize[end] ;i++)
        Desired.push_back(0);
    for(int j = 0; j <  Layersize[end] ;j++)
        Actual.push_back(0);



    for(int output = 0; output < Layersize[end]; output++) {
       desiredoutput = TraineeSamples.OutputValues[pattern][output];
       actualoutput = Output[pattern][output];

    Desired[output] = desiredoutput;


 //  cout<< "desired : "<<desiredoutput<<"      "<<actualoutput<<endl;

     if((actualoutput >= 0)&&(actualoutput <= 0.2))
    actualoutput = 0;

     else if((actualoutput <= 1)&&(actualoutput >= 0.8))
    actualoutput = 1;

      Actual[output] =  actualoutput;

       }
     int confirm = 0;
    for(int b = 0; b <  Layersize[end] ;b++){
     if(Desired[b]== Actual[b] )
      confirm++;
                                            }
    //    cout<<confirm<< " is confirm"<< Layersize[end]<<endl;

         if(confirm == Layersize[end])
            count++;




        confirm = 0;
       // count = 1;
     }
     // cout<<count<< " is count"<<accepted<<endl;
       if(count ==accepted)
        return false;


  return true;
}

bool NeuralNetwork::ErrorToleranceHalf(TrainingExamples TraineeSamples,Sizes Layersize, double TrainStopPercent)
{
    double count = 0;
    int total = TraineeSamples.Datapoints;
    double accepted = total;

    double Error;
    int end = Layersize.size() - 1;

 for(int pattern = 0; pattern< TraineeSamples.Datapoints; pattern++){
    for(int output = 0; output < Layersize[end]; output++) {

      Error = TraineeSamples.OutputValues[pattern][output] - Output[pattern][output];

    if(Error < 0)
     Error = Error * -1;//absolute value of error



    if( Error <= 0.499)
      count ++;





       }//for

     }//for

    double TrainPercent = 0;

    TrainPercent=  (count/ accepted)* 100;

       if(TrainPercent >=TrainStopPercent)
        return false;

     cout<<count<<"  is half the count out of "<<accepted <<" ::"<<TrainPercent<<"  percent"<<endl;

    return true;
}
double NeuralNetwork::SumSquaredError(TrainingExamples TraineeSamples,Sizes Layersize)
{   int end = Layersize.size() - 1;
    double Sum = 0;
    double Error=0;
    double ErrorSquared = 0;
    for(int pattern = 0; pattern< TraineeSamples.Datapoints ; pattern++){
    for(int output = 0; output < Layersize[end]; output++) {
      Error = TraineeSamples.OutputValues[pattern][output] - Output[pattern][output];
    // cout<<"                            "<<TraineeSamples.OutputValues[pattern][output]<< " - "<<Output[pattern][output]<<endl;
  //    cout<<Error<< " : Is Error"<<endl;
      ErrorSquared += (Error * Error);
       }

        Sum += (ErrorSquared);
        ErrorSquared = 0;
  // cout<<"--------------------------------"<<endl;
}
  return Sum/TraineeSamples.Datapoints*Layersize[end];
 //return Sum;
 }

double NeuralNetwork::Rate(TrainingExamples TraineeSamples,Sizes Layersize)
{   int end = Layersize.size() - 1;
    double Sum = 0;
    double Error=0;
    double ErrorSquared=0;
    double rate = 0;
    for(int pattern = 0; pattern< TraineeSamples.Datapoints ; pattern++){
    for(int output = 0; output < Layersize[end]; output++) {
      Error = TraineeSamples.OutputValues[pattern][output] - Output[pattern][output];
    // cout<<"                            "<<TraineeSamples.OutputValues[pattern][output]<< " - "<<Output[pattern][output]<<endl;
  //    cout<<Error<< " : Is Error"<<endl;
      ErrorSquared += (Error * Error);

       }
      Sum += (ErrorSquared);
      ErrorSquared=0;
    //Sum = 0;

  // cout<<"--------------------------------"<<endl;
}

rate =  Sum / (TraineeSamples.Datapoints *(Layersize[end]));
  return rate;
}



void NeuralNetwork::PrintWeights(Sizes Layersize)
{
      int end = Layersize.size() - 1;

    for(int layer=0; layer < Layersize.size()-1; layer++){

  cout<<layer<<"  Weights::"<<endl<<endl;
   for(int row  = 0; row <Layersize[layer] ; row ++){
    for(int col = 0; col < Layersize[layer+1]; col++)
     cout<<nLayer[layer].Weights[row ][col]<<" ";
      cout<<endl;
                }
    cout<<endl<<layer<<"  WeightsChange::"<<endl<<endl;

   for( int row  = 0; row <Layersize[layer] ; row ++){
    for( int col = 0; col < Layersize[layer+1]; col++)
     cout<<nLayer[layer].WeightChange[row ][col]<<" ";
      cout<<endl;
                }
  cout<<"--------------"<<endl;
  }
  //--------------------------------------------
for(int layer=0; layer < Layersize.size() ; layer++){
cout<<endl<<layer<<"  Outputlayer::"<<endl<<endl;
for( int row = 0; row < Layersize[layer] ; row ++)
         cout<<nLayer[layer].Outputlayer[row] <<" ";
cout<<endl<<layer<<"  Bias::"<<endl<<endl;
for( int row = 0; row < Layersize[layer] ; row ++)
         cout<<nLayer[layer].Bias[row] <<" ";
cout<<endl<<layer<<"  Error::"<<endl<<endl;
for(int  row = 0; row < Layersize[layer] ; row ++)
         cout<<nLayer[layer].Error[row] <<" ";
cout<<"----------------"<<endl;

}

// for(int patternNum = 0; patternNum < rowSize ; patternNum ++) {
    //  for(int output = 0; output < Layersize[end]; output++)
  // cout<<Output[patternNum][output]<<" ";
  // cout<<endl;
//}
     }
//-------------------------------------------------------

 void NeuralNetwork::SaveLearnedData(Sizes Layersize, char* filename)
  {

	ofstream out;
	out.open(filename);
	if(!out) {
    cout << endl << "failed to save file" << endl;
    return;
    }
   //-------------------------------
    for(int layer=0; layer < Layersize.size()-1; layer++){
         for(int row  = 0; row <Layersize[layer] ; row ++){
          for(int col = 0; col < Layersize[layer+1]; col++)
            out<<nLayer[layer].Weights[row ][col]<<" ";
              out<<endl;
                }
              out<<endl;
                 }
  //--------------------------------
  // output bias.
   for(int  layer=1; layer < Layersize.size(); layer++){
     for(int y = 0 ; y < Layersize[layer]; y++) {
	  out<<	nLayer[layer].Bias[y]<<"  ";
	   out<<endl<<endl;
        }
	     out<<endl;
           }
  //------------------------------
	  out.close();
	//cout << endl << "data saved" << endl;

	return;
}

 void NeuralNetwork::LoadSavedData(Sizes Layersize,char* filename)
{
 	ifstream in(filename);
    if(!in) {
    cout << endl << "failed to save file" << endl;
    return;
    }
     //-------------------------------
  for(int layer=0; layer < Layersize.size()-1; layer++)
     for(int row  = 0; row <Layersize[layer] ; row ++)
       for(int col = 0; col < Layersize[layer+1]; col++)
         in>>nLayer[layer].Weights[row ][col];


  //--------------------------------
  // output bias.
   for( int layer=1; layer < Layersize.size(); layer++)
     for(int y = 0 ; y < Layersize[layer]; y++)
	    in>>	nLayer[layer].Bias[y] ;

  //------------------------------

  in.close();
//   cout << endl << "data loaded for testing" << endl;

	return;
 }

   double NeuralNetwork::CountTestingData(TrainingExamples TraineeSamples,int temp,Sizes Layersize)
{
    double count = 0;
    int total = TraineeSamples.Datapoints;
    double accepted =  temp * 1;

    double desiredoutput;
    double actualoutput;

    double Error;
    int end = Layersize.size() - 1;

    for(int pattern = 0; pattern< temp; pattern++){

    Layer Desired;
    Layer Actual;

    for(int i = 0; i <  Layersize[end] ;i++)
        Desired.push_back(0);
    for(int j = 0; j <  Layersize[end] ;j++)
        Actual.push_back(0);



    for(int output = 0; output < Layersize[end]; output++) {
       desiredoutput = TraineeSamples.OutputValues[pattern][output];
       actualoutput = Output[pattern][output];

    Desired[output] = desiredoutput;


  //  cout<< "desired : "<<desiredoutput<<"      "<<actualoutput<<endl;

     if((actualoutput >= 0)&&(actualoutput <= 0.5))
    actualoutput = 0;

     else if((actualoutput <= 1)&&(actualoutput >= 0.5))
    actualoutput = 1;

      Actual[output] =  actualoutput;

       }
     int confirm = 0;
    for(int b = 0; b <  Layersize[end] ;b++){
     if(Desired[b]== Actual[b] )
      confirm++;
                                                 }
      if(confirm == Layersize[end])
      count++;

      confirm = 0;

     }



  return count;

}

double NeuralNetwork::CountLearningData(TrainingExamples TraineeSamples,int temp,Sizes Layersize)
{
    double count = 0;
    int total = TraineeSamples.Datapoints;
    double accepted =  temp * 1;

    double desiredoutput;
    double actualoutput;

    double Error;
    int end = Layersize.size() - 1;

    for(int pattern = 0; pattern< temp; pattern++){

    Layer Desired;
    Layer Actual;

    for(int i = 0; i <  Layersize[end] ;i++)
        Desired.push_back(0);
    for(int j = 0; j <  Layersize[end] ;j++)
        Actual.push_back(0);



    for(int output = 0; output < Layersize[end]; output++) {
       desiredoutput = TraineeSamples.OutputValues[pattern][output];
       actualoutput = Output[pattern][output];

    Desired[output] = desiredoutput;


  //  cout<< "desired : "<<desiredoutput<<"      "<<actualoutput<<endl;

     if((actualoutput >= 0)&&(actualoutput <= 0.2))
    actualoutput = 0;

     else if((actualoutput <= 1)&&(actualoutput >= 0.8))
    actualoutput = 1;

      Actual[output] =  actualoutput;

       }
     int confirm = 0;
    for(int b = 0; b <  Layersize[end] ;b++){
     if(Desired[b]== Actual[b] )
      confirm++;
                                                 }
      if(confirm == Layersize[end])
      count++;

      confirm = 0;

     }



  return count;

}
bool NeuralNetwork::CheckOutput(TrainingExamples TraineeSamples,int pattern,Sizes Layersize)
{

       int end = Layersize.size() - 1;
    double desiredoutput;
    double actualoutput;


    Layer Desired;
    Layer Actual;

    for(int i = 0; i <  Layersize[end] ;i++)
        Desired.push_back(0);
    for(int j = 0; j <  Layersize[end] ;j++)
        Actual.push_back(0);




    int count = 0;
     //for(int patt  = 0;patt < pattern ;patt ++){
     for(int output = 0; output < Layersize[end]; output++) {
       desiredoutput = TraineeSamples.OutputValues[pattern][output];
       actualoutput = Output[pattern][output];
    Desired[output] = desiredoutput;



      cout<< "desired : "<<desiredoutput<<"      "<<actualoutput<<endl;


     if((actualoutput >= 0)&&(actualoutput <= 0.5))
    actualoutput = 0;

     else if((actualoutput <= 1)&&(actualoutput >= 0.5))
    actualoutput = 1;

      Actual[output] =  actualoutput;
      }


     cout<<"---------------------"<<endl;


     for(int b = 0; b <  Layersize[end] ;b++){
     if(Desired[b]!= Actual[b] )
        return false;
    }
  return true;
}

double NeuralNetwork::TestTrainingData(Sizes Layersize, char* filename,int  size, char* load, int inputsize, int outputsize  )
{
     bool valid;
     double count = 1;
     double total;
     double accuracy;

     TrainingExamples Test(filename,size,inputsize+outputsize ,   inputsize,  outputsize );
   //  Test.printData();
     total = Test.InputValues.size(); //how many samples to test?

     CreateNetwork(Layersize,Test);

     LoadSavedData(Layersize,load);

    // PrintWeights(Layersize);

     for(int pattern = 0;pattern < total ;pattern++){

      ForwardPass(Test,pattern,Layersize);
      }
    //  count = CheckOutput( Test,total,Layersize);

     // if(valid == true)
       // count++;

//}
count = CountLearningData(Test,size,Layersize);

accuracy = (count/total)* 100;
 //cout<<"The sucessful count is "<<count<<" out of "<<total<<endl;
 //cout<<"The accuracy of test is: " <<accuracy<<" %"<<endl;
 //  cout<<"ramstein"<<endl;
return accuracy;


//accuracy = (count/total)* 100;
//cout<<"The sucessful count is "<<count<<" out of "<<total<<endl;
//cout<<"The accuracy of test is: " <<accuracy<<" %"<<endl;

//return accuracy;

 }
double NeuralNetwork::TestLearnedData(Sizes Layersize, char* filename,int  size, char* load, int inputsize, int outputsize )
{
     bool valid;
     double count = 1;
     double total;
     double accuracy;

     TrainingExamples Test(filename,size,inputsize+outputsize, inputsize, outputsize);
   //  Test.printData();
     total = Test.InputValues.size(); //how many samples to test?

     CreateNetwork(Layersize,Test);

     LoadSavedData(Layersize,load);

    // PrintWeights(Layersize);

     for(int pattern = 0;pattern < total ;pattern++){

      ForwardPass(Test,pattern,Layersize);
      }
    //  count = CheckOutput( Test,total,Layersize);

     // if(valid == true)
       // count++;

//}
count = CountTestingData(Test,size,Layersize);

accuracy = (count/total)* 100;
 cout<<"The sucessful count is "<<count<<" out of "<<total<<endl;
 cout<<"The accuracy of test is: " <<accuracy<<" %"<<endl;
 //  cout<<"ramstein"<<endl;
return accuracy;


//accuracy = (count/total)* 100;
//cout<<"The sucessful count is "<<count<<" out of "<<total<<endl;
//cout<<"The accuracy of test is: " <<accuracy<<" %"<<endl;

//return accuracy;

 }
 
double NeuralNetwork::ForwardPassP(TrainingExamples TraineeSamples,int patternNum,Sizes Layersize)
{

      double WeightedSum = 0;
      double ForwardOutput;




      int end = Layersize.size() - 1;

      for(int row = 0; row < Layersize[0] ; row ++)
       nLayer[0].Outputlayer[row] = TraineeSamples.InputValues[patternNum][row];



    // out<<neuron12<<" is 12 "<<neuron29<<" is 29 "<<neuron30<<" is 30"<<endl;
    //--------------------------------------------
   for(int layer=0; layer < Layersize.size()-1; layer++){

 for(int y = 0; y< Layersize[layer+1]; y++) {
    for(int x = 0; x< Layersize[layer] ; x++){
       	WeightedSum += (nLayer[layer].Outputlayer[x] * nLayer[layer].Weights[x][y]);

        ForwardOutput = WeightedSum - nLayer[layer+1].Bias[y];
     }
  nLayer[layer+1].Outputlayer[y] = Sigmoid(ForwardOutput);


  //  cout<<nLayer[layer+1].Outputlayer[y]<<" is Output of "<<layer+1<<endl;
 WeightedSum = 0;
 }
 WeightedSum = 0;
}//end layer

   //--------------------------------------------
   for(int output= 0; output < Layersize[end] ; output ++)
    Output[patternNum][output] = nLayer[end].Outputlayer[output];


    double temp = 0;
    //----------------------------------------
    //  for(int row = 0; row < Layersize[0] ; row ++)
      // nLayer[0].Outputlayer[row] = TraineeSamples.InputValues[patternNum][row];
    double tmpo =0;
    double Rules =0;
    double NoRules =0;
    double WeightedError =0;
   // int layer =0;
   double B = 0;

   for(int layer=0; layer < Layersize.size()-1; layer++){

    for( y = 0; y< Layersize[layer+1]; y++) {
    for(  x = 0; x< Layersize[layer] ; x++){

     if( nLayer[layer].H[x][y] == 1){
   	Rules  = (nLayer[layer].Outputlayer[x] * 1);
   	 //cout<<Rules<<" is Rules"<<endl;
        }

  //  if( nLayer[layer].H[x][y] == 0)
   //	NoRules  = (nLayer[layer].Outputlayer[x] * 0);

   	if(nLayer[layer+1].Gates[y]== 1)
   	 B = 1;
      // cout<<B<<" is B"<<endl;}
     WeightedError = (nLayer[layer].Error[x] *  nLayer[layer].Weights[x][y]);
   //  cout<<WeightedError<<" is Weighted error"<<endl;


     nLayer[layer+1].Error[y] =  (nLayer[layer+1].Outputlayer[y]*(1-nLayer[layer+1].Outputlayer[y]) *( (Rules + WeightedError) - B));
     }
    }

}
    double Herror ;

      for(int output = 0; output < Layersize[end]; output++) {
   //  cout<< nLayer[end].Error[output]<<"  er"<<endl;
 // Herror = (-1*(TraineeSamples.OutputValues[patternNum][0] - Output[patternNum][0])*nLayer[end].Error[output]);
 Herror = (-1*(TraineeSamples.OutputValues[patternNum][0] - Output[patternNum][0]) );

}
 return Herror;
 }




Layer NeuralNetwork::BackpropogationCICC(Layer ind, TrainingExamples TraineeSamples, double LearningRate,Sizes Layersize,char* Savefile,bool load, int EpochMax, ofstream &out1)
{

	/// ---------------- Backpropagation Algorithm island procedure ---------------------- ///
	double SumErrorSquared;

    //CreateNetwork(Layersize,TraineeSamples );
	ChoromesToNeurons(ind); //Populate network with individual


	int Id = 0;
	int Epoch = 0;

	bool Learn = true;

	while( Learn == true)
	{

		for(int pattern = 0; pattern < TraineeSamples.InputValues.size(); pattern++)
		{
			ForwardPass(TraineeSamples,pattern,Layersize);
			BackwardPass(TraineeSamples,0.1,pattern,Layersize);
		}


		Epoch++;
		//cout<<Epoch<< " : is Epoch    *********************    "<<endl;

	  SumErrorSquared = SumSquaredError(TraineeSamples,Layersize);
		cout<<SumErrorSquared<< " : is SumErrorSquared"<<endl;
		//out1<<SumErrorSquared<< " : is SumErrorSquared"<<endl;

//
//
//
   	  SaveLearnedData(Layersize, Savefile);
//
 	//double trained =  TestTrainingData(Layersize,testfile,testsize, Savefile, Layersize[0], Layersize[2]);
//
//		cout<<trained<<" is percentage trained"<<endl;

		//if(trained>=98)
		//{
			//Learn = false;

		//}

		if(Epoch ==  EpochMax )
		Learn = false;


	}


	/*gene = network.BackPropogation(  Samples,0.1,layersize, file, true);*/
	//Train =  network.TestTrainingData(layersize,trainfile,trainsize, file, input, output);
	//Test =  network.TestLearnedData(layersize,testfile,testsize, file, input, output); //test the network


	/*for( int cycle = 0; cycle <(neutral - 4); cycle++)
	 {
		LayerLevel.EvolveSubPopulations(NeuronLevel.NoSpecies-1  , 1 ,network, Samples,mutation,0,out2);
		LayerLevel.GetBestTableGetBestTableGetBestTable(LayerLevel.NoSpecies-1);
		LayerLevel.Join();
		network.ChoromesToNeurons(LayerLevel.Individual);
		network.SaveLearnedData(layersize, file);
		//NeuronLevelTrain =   network.TestTrainingData(layersize,trainfile,trainsize, file, input, output );
		NetLevelError= LayerLevel.Species[LayerLevel.NoSpecies-1].Population[LayerLevel.Species[LayerLevel.NoSpecies-1].BestIndex].Fitness;

	}*/
         //double BestBP=   SumErrorSquared;


	//cout<<  "Backpropagation Island Epochs:  "<<Epoch << " Error: " <<  BestBP <<endl;
    // LoadSavedData(Layersize,Savefile);

    ind = Neurons_to_chromes();

	return ind; //Save network weights back to ind and return


	//network.CreateNetwork(layersize,Samples ); // Create network once for CICC competition between all methods
}
 

 int NeuralNetwork::BackPropogation(  TrainingExamples TraineeSamples, double LearningRate,Sizes Layersize, char * Savefile, bool load)
{
     double SumErrorSquared;

     CreateNetwork(Layersize,TraineeSamples );

   //if( load == true)
    // LoadSavedData(Layersize,Savefile);

   int Id = 0;
   int Epoch = 0;

   bool Learn = true;

    while( Learn == true){

     for(int pattern = 0; pattern < TraineeSamples.InputValues.size(); pattern++)
      {
     ForwardPass( TraineeSamples,pattern,Layersize);

    BackwardPass(TraineeSamples,LearningRate,pattern,Layersize);


       }


    Epoch++;
    cout<<Epoch<< " : is Epoch    *********************    "<<endl;

     SumErrorSquared = SumSquaredError(TraineeSamples,Layersize);
   cout<<SumErrorSquared<< " : is SumErrorSquared"<<endl;


   // Learn =  ErrorTolerance(TraineeSamples,Layersize,100);
   //double count = CountLearningData(TraineeSamples,TraineeSamples.InputValues.size(), Layersize);
 //  SaveLearnedData(Layersize, Savefile);
   double trained =  TestTrainingData(Layersize,"iris.csv",108, Savefile, Layersize[0], Layersize[2]);
 //double trained= count/TraineeSamples.InputValues.size()*100;
  cout<<trained<<" is percentage trained"<<endl;

  if(trained>=98){
        Learn = false;

                   }

//   ErrorToleranceHalf(TraineeSamples,Layersize,87);

   if(Epoch ==  4000  )
     Learn = false;


}


 return Epoch;
}
//flag
Layer NeuralNetwork::  Neurons_to_chromes(  )
{
    int gene = 0;
     Layer NeuronChrome(StringSize);
 
int layer = 0;
      
#ifdef neuronlevel
         for(int neu = 0; neu < layersize[1]; neu++ ){
 
             for( int row = 0; row<  layersize[layer] ; row++){
                     NeuronChrome[gene]=nLayer[layer].Weights[row][neu]  ;
                  gene++;   }
 
                 NeuronChrome[gene] = nLayer[layer+1 ].Bias[neu]  ;
                   gene++;
                  }
 
          layer = 1;
 
         for(int neu = 0; neu < layersize[2]; neu++ ){
 
                     for( int row = 0; row<  layersize[layer] ; row++){
                          NeuronChrome[gene]  = nLayer[layer].Weights[row][neu] ;
                          gene++;   }
 
                         NeuronChrome[gene] = nLayer[layer+1 ].Bias[neu] ;
                           gene++;
         }
 
 
#endif 
 
#ifdef onelevel
       for(int layer=0; layer <  layersize.size() ; layer++){
        for( row = 0; row <  layersize[layer] ; row ++) {
       NeuronChrome[gene]  =  nLayer[layer ].Bias[row];
           gene++; }
 
 
           }
     for(int layer=0; layer <  layersize.size()-1; layer++){
         for( row = 0; row<  layersize[layer] ; row++){
            for( col = 0; col <  layersize[layer+1]; col++)  {
      NeuronChrome[gene] =  nLayer[layer].Weights[row][col];
      gene++; } }}
    //---------------------------------------------------------
 
    //-----------------------------------------------

#endif
 
   return   NeuronChrome;
}
 
 
void NeuralNetwork:: ChoromesToNeurons(  Layer NeuronChrome)
{
     StringSize = NeuronChrome.size();
    int layer = 0;
    int gene = 0;
#ifdef neuronlevel
         for(int neu = 0; neu < layersize[1]; neu++ ){
 
             for( int row = 0; row<  layersize[layer] ; row++){
                  nLayer[layer].Weights[row][neu]=  NeuronChrome[gene]  ;
                  gene++;   }
 
                 nLayer[layer+1 ].Bias[neu] =   NeuronChrome[gene]  ;
                   gene++;
                  }
 
          layer = 1;
 
         for(int neu = 0; neu < layersize[2]; neu++ ){
 
                     for( int row = 0; row<  layersize[layer] ; row++){
                          nLayer[layer].Weights[row][neu]=  NeuronChrome[gene]  ;
                          gene++;   }
 
                         nLayer[layer+1 ].Bias[neu] =   NeuronChrome[gene]  ;
                           gene++;
         }
 
 
#endif
 
#ifdef hiddenNeuron
         for(int neu = 0; neu < layersize[1]; neu++ ){
 
             for( int row = 0; row<  layersize[layer] ; row++){
                  nLayer[layer].Weights[row][neu]=  NeuronChrome[gene]  ;
                  gene++;   }
 
                 nLayer[layer+1 ].Bias[neu] =   NeuronChrome[gene]  ;
                   gene++;
 
                   for( int row = 0; row<  layersize[layer+2] ; row++){
                              nLayer[layer+1].Weights[neu][row]=  NeuronChrome[gene]  ;
                                gene++;}
 
         }
 
 
#endif
    //int gene = 0;
 
#ifdef weightlevel
 
     for(int layer=0; layer <  layersize.size()-1; layer++){
         for( row = 0; row<  layersize[layer] ; row++){
            for( col = 0; col <  layersize[layer+1]; col++)  {
      nLayer[layer].Weights[row][col]=  NeuronChrome[gene]  ;
      gene++; } }}
    //---------------------------------------------------------
 
    //-----------------------------------------------
      for(int layer=0; layer <  layersize.size() ; layer++){
        for( row = 0; row <  layersize[layer] ; row ++) {
         nLayer[layer ].Bias[row] =   NeuronChrome[gene]  ;
           gene++; }
 
 
           }
#endif
 
 
#ifdef onelevel
    for(int layer=0; layer <  layersize.size() ; layer++){
        for( row = 0; row <  layersize[layer] ; row ++) {
         nLayer[layer ].Bias[row] =   NeuronChrome[gene]  ;
           gene++; }
 
 
           }
     for(int layer=0; layer <  layersize.size()-1; layer++){
         for( row = 0; row<  layersize[layer] ; row++){
            for( col = 0; col <  layersize[layer+1]; col++)  {
      nLayer[layer].Weights[row][col]=  NeuronChrome[gene]  ;
      gene++; } }}
    //---------------------------------------------------------
 
    //-----------------------------------------------
   
#endif
 
 
}
double NeuralNetwork::ForwardFitnessPass(  Layer NeuronChrome,   TrainingExamples Test)

{

         ChoromesToNeurons( NeuronChrome);


NumEval++;

      // CreateNetwork(layersize,Test);
      // CreateNetwork(layersize,Test.SampleSize);

      double SumErrorSquared = 0;
      bool Learn=true;


    for(int pattern = 0; pattern < Test.InputValues.size(); pattern++)
      {
     ForwardPass( Test,pattern,layersize);

     }
    // for(int pattern = 0; pattern < Test.InputValues.size(); pattern++)
     // {
     // BackwardPass(Test,.1,pattern,layersize);
     //send neurons to chrome.......
      // }
 //    ChromeNeuron = Neurons_to_chromes();

    SumErrorSquared = SumSquaredError(Test,layersize);

    // Learn =  ErrorTolerance(Test,layersize,95);

      if(Learn == false )
         return -1;

  return  SumErrorSquared ;

}

double NeuralNetwork::BP(  Layer NeuronChrome,   TrainingExamples Test, int generations)

{
 
      double SumErrorSquared = 0;
         bool Learn=true;


       //CreateNetwork(layersize,Test);
        //CreateNetwork(layersize,Test.SampleSize);
ChoromesToNeurons( NeuronChrome);
     



    for(int epoch = 0; epoch < generations; epoch++){

  for(int pattern = 0; pattern < Test.InputValues.size(); pattern++)
      {
     ForwardPass( Test,pattern,layersize);

     }
      for(int pattern = 0; pattern < Test.InputValues.size(); pattern++)
       {
       BackwardPass(Test,.1,pattern,layersize);
     //send neurons to chrome.......
       }


   
     }
  SumErrorSquared = SumSquaredError(Test,layersize);




   // SaveLearnedData(layersize, "Learnt.txt");
 	 
   ChromeNeuron = Neurons_to_chromes();
  return  SumErrorSquared ;

}

//-------------------------------------------------------
//-------------------------------------------------------

class Individual{

   //  friend class GeneticAlgorithmn;
   // friend class CoEvolution;
  //    friend class CombinedEvolution;
       public:

        Layer Chrome;
        double Fitness;
        Layer BitChrome;

       public:
        Individual()
        {

        }
        void print();


      };
//***************************************************
//typedef vector <Individual> Pop ;
typedef vector<double> Nodes;
//class GeneticAlgorithmn:RandomNumber
  class GeneticAlgorithmn :public virtual RandomNumber  // , public virtual NeuralNetwork, public virtual TrainingExamples
  {
	//friend class Individual;
   // friend class TrainingExamples;
    //    friend class NeuralNetwork;
     //   friend class  Layers;
      //  friend class CoEvolution;
       // friend class CombinedEvolution;
       public:

        int PopSize;

     	vector<Individual> Population;

   	 
     	Sizes TempIndex ;

     	vector<Individual> NewPop; 

     	Sizes mom ;
     	Sizes list;

              int MaxGen;


       int NumVariable;

       double BestFit;
       int BestIndex;
       int NumEval;

       int  kids;

       public:
        GeneticAlgorithmn(int stringSize )
        {
     	   NumVariable = stringSize;
          NumEval=0;
           BestIndex = 0;
     
         }
        GeneticAlgorithmn()
       {
             BestIndex = 0;
        }



       double Fitness() {return BestFit;}

       double  RandomWeights();


       double  RandomAddition();

       void PrintPopulation();


       int GenerateNewPCX(int pass,NeuralNetwork network,TrainingExamples Sample, double Mutation, int depth);

       double Objective(Layer x);

       void  InitilisePopulation(int popsize);

       void Evaluate();


       double  modu(double index[]);



       // calculates the inner product of two vectors

       double  innerprod(double Ind1[],double Ind2[]);

       double RandomParents();

       double MainAlgorithm(double RUN, ofstream &out1, ofstream &out2, ofstream &
	   out3);

       double Noise();

       void  my_family();   //here a random family (1 or 2) of parents is created who would be replaced by good individuals

       void  find_parents() ;
       void  rep_parents() ;  //here the best (1 or 2) individuals replace the family of parents
       void sort();

   };

   //-------------------------------


double GeneticAlgorithmn::RandomWeights()
{
      int chance;
      double randomWeight;
      double NegativeWeight;
      chance =rand()%2;

      if(chance ==0){
      randomWeight =rand()%  100000;
      return randomWeight*0.00005;
       }

      if(chance ==1){
      NegativeWeight =rand()% 100000;
      return NegativeWeight*-0.00005;
     }

}

double GeneticAlgorithmn::RandomAddition()
{
      int chance;
      double randomWeight;
      double NegativeWeight;
      chance =rand()%2;

      if(chance ==0){
      randomWeight =rand()% 100;
      return randomWeight*0.009;
       }

      if(chance ==1){
      NegativeWeight =rand()% 100;
      return NegativeWeight*-0.009;
     }

}



void GeneticAlgorithmn::InitilisePopulation(int popsize)
    {

double x, y;

	//vector<Individual> PopulationP(popsize);

     	//vector<Individual> NewPopP(NPSize); 
Individual Indi ;

	   NumEval=0;
           BestIndex = 0;
           PopSize = popsize;

    //   Population =PopulationP;
    //   NewPop = NewPopP;    

      for (int i = 0; i < PopSize; i++){ 
         TempIndex.push_back(0);
         mom.push_back(0); }

      for (int i = 0; i < NPSize; i++){  	
         list.push_back(0);}

 for(int row = 0; row < PopSize  ; row++) 
        Population.push_back(Indi);

  for(int row = 0; row < PopSize  ; row++) {
   for(int col = 0; col < NumVariable ; col++){
  
      Population[row].Chrome.push_back(RandomWeights());}
  }


 for(int row = 0; row < NPSize  ; row++) 
        NewPop.push_back(Indi);

  for(int row = 0; row < NPSize  ; row++) {
     for(int col = 0; col < NumVariable ; col++)
        NewPop[row].Chrome.push_back(0);

  }
     
  }

void GeneticAlgorithmn::Evaluate()
{
	// solutions are evaluated and best id is computed

	  Population[0].Fitness= Objective( Population[0].Chrome);
       BestFit = Population[0].Fitness;
       BestIndex = 0;

        for(int row = 0; row < PopSize  ; row++)
        {
          Population[row].Fitness= Objective( Population[row].Chrome);
          if ((MINIMIZE * BestFit) > (MINIMIZE * Population[row].Fitness))
          	{
        	  BestFit = Population[row].Fitness;
        	  BestIndex = row;
          	}
        }

}


void GeneticAlgorithmn::PrintPopulation()
{
	 for(int row = 0; row < PopSize/5   ; row++) {
	   for(int col = 0; col < NumVariable ; col++)
	      cout<< Population[row].Chrome[col]<<" ";
	      cout<<endl;
	}

	  for(int row = 0; row < PopSize/5  ; row++)
		  cout<< Population[row].Fitness<<endl;

	 cout<<" ---"<<endl;
	 cout<<BestFit<<"  "<<BestIndex<<endl;

		/*for(int row = 0; row < NPSize  ; row++) {
		   for(int col = 0; col < NumVariable ; col++)
		      cout<< NewPop[row].Chrome[col]<<" ";
		      cout<<endl;
		}*/

}


double GeneticAlgorithmn:: Objective(Layer x)
{
 
  return 0;
}
//------------------------------------------------------------------------

void GeneticAlgorithmn:: my_family()   //here a random family (1 or 2) of parents is created who would be replaced by good individuals
{
  int i,j,index;
  int swp;
  double u;

  for(i=0;i<PopSize;i++)
    mom[i]=i;

  for(i=0;i<family;i++)
    {
   //   u=randomperc();
  //    index=(u*(PopSize-i))+i;

	  index = (rand()%PopSize) +i;

	 // cout<<"is index  "<<index<<endl;
      if(index>(PopSize-1)) index=PopSize-1;
      swp=mom[index];
      mom[index]=mom[i];
      mom[i]=swp;
    }
}

void GeneticAlgorithmn::find_parents()   //here the parents to be replaced are added to the temporary sub-population to assess their goodness against the new solutions formed which will be the basis of whether they should be kept or not
{
  int i,j,k;
  double u,v;

 my_family();
//cout<<kids<<endl;
  for(j=0;j<family;j++)
    {
      for(i=0;i<NumVariable;i++)
 	NewPop[kids+j].Chrome[i] = Population[mom[j]].Chrome[i];

      NewPop[kids+j].Fitness = Objective(NewPop[kids+j].Chrome);

    }
}



void GeneticAlgorithmn::rep_parents()   //here the best (1 or 2) individuals replace the family of parents
{
  int i,j;
  for(j=0;j<family;j++)
    {
      for(i=0;i<NumVariable;i++)
       Population[mom[j]].Chrome[i]=NewPop[list[j]].Chrome[i];

      Population[mom[j]].Fitness = Objective(Population[mom[j]].Chrome);

    }
}


void GeneticAlgorithmn::sort()

{
  int i,j, temp;
  double dbest;

  for (i=0;i<(kids+family);i++) list[i] = i;

  if(MINIMIZE)
    for (i=0; i<(kids+family-1); i++)
      {
	dbest = NewPop[list[i]].Fitness;
	for (j=i+1; j<(kids+family); j++)
	  {
	    if(NewPop[list[j]].Fitness < dbest)
	      {
		dbest = NewPop[list[j]].Fitness;
		temp = list[j];
		list[j] = list[i];
		list[i] = temp;
	      }
	  }
      }
  else
    for (i=0; i<(kids+family-1); i++)
      {
	dbest = NewPop[list[i]].Fitness;
	for (j=i+1; j<(kids+family); j++)
	  {
	    if(NewPop[list[j]].Fitness > dbest)
	      {
		dbest = NewPop[list[j]].Fitness;
		temp = list[j];
		list[j] = list[i];
		list[i] = temp;
	      }
	  }
      }
}


//---------------------------------------------------------------------
double GeneticAlgorithmn::  modu(double index[])
{
  int i;
  double sum,modul;

  sum=0.0;
  for(i=0;i<NumVariable ;i++)
    sum+=(index[i]*index[i]);

  modul=sqrt(sum);
  return modul;
}

// calculates the inner product of two vectors
double GeneticAlgorithmn::  innerprod(double Ind1[],double Ind2[])
{
  int i;
  double sum;

  sum=0.0;

  for(i=0;i<NumVariable ;i++)
    sum+=(Ind1[i]*Ind2[i]);

  return sum;
}

int GeneticAlgorithmn::GenerateNewPCX(int pass,NeuralNetwork network,TrainingExamples Sample, double Mutation, int depth)
{
  int i,j,num,k;
  double Centroid[NumVariable];
  double tempvar,tempsum,D_not,dist;
  double tempar1[NumVariable];
  double tempar2[NumVariable];
  double D[RandParent];
  double d[NumVariable];
  double diff[RandParent][NumVariable];
  double temp1,temp2,temp3;
  int temp;

  for(i=0;i<NumVariable;i++)
    Centroid[i]=0.0;

  // centroid is calculated here
  for(i=0;i<NumVariable;i++)
    {
      for(j=0;j<RandParent;j++)
	Centroid[i]+=Population[TempIndex[j]].Chrome[i];

      Centroid[i]/=RandParent;
      
  //cout<<Centroid[i]<<" --- "<<RandParent<<"  ";
          // if(isnan(Centroid[i])) return 0;
    }
  //cout<<endl;

  // calculate the distace (d) from centroid to the index parent arr1[0]
  // also distance (diff) between index and other parents are computed
  for(j=1;j<RandParent;j++)
    {
      for(i=0;i<NumVariable;i++)
	{
	  if(j == 1)
	    d[i]=Centroid[i]-Population[TempIndex[0]].Chrome[i];
	  diff[j][i]=Population[TempIndex[j]].Chrome[i]-Population[TempIndex[0]].Chrome[i];
	}
      if (modu(diff[j]) < EPSILON)
	{
	  cout<< "RUN Points are very close to each other. Quitting this run   " <<endl;

	  return (0);
	}

 if (isnan(diff[j][i])  )
	{
	  cout<< "`diff nan   " <<endl;
             diff[j][i] = 1;
	  return (0);
	} 


    }
  dist=modu(d); // modu calculates the magnitude of the vector

  if (dist < EPSILON)
    {
	  cout<< "RUN Points are very close to each other. Quitting this run    " <<endl;

      return (0);
    }

  // orthogonal directions are computed (see the paper)
  for(i=1;i<RandParent;i++)
    {
      temp1=innerprod(diff[i],d);
      if((modu(diff[i])*dist) == 0){
       cout<<" division by zero: part 1"<<endl;
         temp2=temp1/(1);
        }
      else{
      temp2=temp1/(modu(diff[i])*dist);}

      temp3=1.0-pow(temp2,2.0);
      D[i]=modu(diff[i])*sqrt(temp3);
    }

  D_not=0;
  for(i=1;i<RandParent;i++)
    D_not+=D[i];

  D_not/=(RandParent-1); //this is the average of the perpendicular distances from all other parents (minus the index parent) to the index vector

  // Next few steps compute the child, by starting with a random vector
  for(j=0;j<NumVariable;j++)
    {
      tempar1[j]=noise(0.0,(D_not*sigma_eta));
      //tempar1[j] = Noise();
      tempar2[j]=tempar1[j];
    }

  for(j=0;j<NumVariable;j++)
    {
      if ( pow(dist,2.0)==0){
     cout<<" division by zero: part 2"<<endl;
       tempar2[j] = tempar1[j]-((innerprod(tempar1,d)*d[j])/1);
   }
      else
      tempar2[j] = tempar1[j]-((innerprod(tempar1,d)*d[j])/pow(dist,2.0));
    }

  for(j=0;j<NumVariable;j++)
    tempar1[j]=tempar2[j];

  for(k=0;k<NumVariable;k++)
    NewPop[pass].Chrome[k]=Population[TempIndex[0]].Chrome[k]+tempar1[k];

 tempvar=noise(0.0,(sigma_zeta));

 // tempvar =Noise();


  for(k=0;k<NumVariable;k++){ 
      NewPop[pass].Chrome[k] += (tempvar*d[k]);
   
}

  double random = rand()%10;

Layer Chrome(NumVariable);

for(k=0;k<NumVariable;k++){
if(!isnan(NewPop[pass].Chrome[k] )){
 Chrome[k]=NewPop[pass].Chrome[k] ;
}
else 
  NewPop[pass].Chrome[k] =  RandomAddition();
  
   }


  //if(random<1){
	//  for(k=0;k<NumVariable;k++)
	    //   NewPop[pass].Chrome[k] += RandomAddition();
 // }
  // the child is included in the newpop and is evaluated
  ///cout<< NewPop[pass].Fitness << "        pop fit"<<endl;
    // NewPop[pass].Fitness =  Objective(NewPop[pass].Chrome);
  ///  cout<< NewPop[pass].Fitness << "is new pop fit"<<endl;

//  double sandom = rand()%100;

  // if( sandom < (100*Mutation)){
    //  double sum =    network.BP( Chrome, Sample,depth) ;
  //NewPop[pass].Chrome =  network.ChromeNeuron ;
   //}



  return (1);
}




//------------------------------------------------------------------------
double GeneticAlgorithmn::  RandomParents()
{

	int i,j,index;
	  int swp;
	  double u;
	  int delta;

	  for(i=0;i<PopSize;i++)
	    TempIndex[i]=i;

	  swp=TempIndex[0];
	  TempIndex[0]=TempIndex[BestIndex];  // best is always included as a parent and is the index parent
	                       // this can be changed for solving a generic problem
	 TempIndex[BestIndex]=swp;

	  for(i=1;i<RandParent;i++)  // shuffle the other parents
	    {
	    //u=randomperc();
	    index=(rand()%PopSize)+i;

	    if(index>(PopSize-1)) index=PopSize-1;
	    swp=TempIndex[index];
	    TempIndex[index]=TempIndex[i];
	    TempIndex[i]=swp;
	    }


}


void TIC( void )
{ TicTime = time(NULL); }

void TOC( void )
{ TocTime = time(NULL); }

double StopwatchTimeInSeconds()
{ return difftime(TocTime,TicTime); }



double GeneticAlgorithmn:: MainAlgorithm(double RUN, ofstream &out1, ofstream &out2, ofstream &out3 )
{
	  TIC();
	double tempfit =0;
    int count =0;
    int tag;
      kids = KIDS;

    int gen = MAXFUN/kids;
gen = 300000;
    basic_seed=0.4122;   //arbitrary choice
/*
    out2<<"             Initial Parameters \n\n";
    out2<<"Population size : \n"<<PopSize;
    out2<<"Number of variables : \n"<<NumVariable;
    out2<<"Pool size of kids formed by PCX : \n"<<KIDS;
    out2<<"Number of parents participating in PCX : \n"<<RandParent;
     out2<<"Number of parents to be replaced by good kids : \n"<<family;
     out2<<"Sigma eta :  \n"<<sigma_eta ;
     out2<<"Sigma zeta : \n"<<sigma_zeta ;
     out2<<"Best fitness required :  \n"<<LIMIT ;
     out2<<"Number of runs desired :  \n\n"<<MAXRUN ;
*/
    //for(RUN=1;RUN<=MAXRUN;RUN++)
   //   {

  //	  printf("run... ");
        seed=basic_seed+(1.0-basic_seed)*(double)(RUN-1)/(double)MAXRUN;
        if(seed>1.0) printf("\n warning!!! seed number exceeds 1.0");
          randomize(seed);

	InitilisePopulation(100);
    Evaluate();

    tempfit=Population[BestIndex].Fitness;

   for(count=1;((count<=gen)&&(tempfit>=LIMIT));count++)
   	{
    //   cout<<count<<endl;



	    RandomParents();           //random array of parents to do PCX is formed

	  // for(int i = 0; i < PopSize; i++)
         //  cout<<TempIndex[i]<< " ";
         //  cout<<endl;

	    for(int i=0;i<kids;i++)
	   	     {
	   	   //  tag = GenerateNewPCX(i,network, Samples); //generate a child using PCX
	   	    if (tag == 0) break;
	   	     }
	   	   if (tag == 0) break;
	   	// PrintPopulation();

	        find_parents();  // form a pool from which a solution is to be
	   	                           //   replaced by the created child

	   		   sort();          // sort the kids+parents by fitness

	   		   rep_parents();   // a chosen parent is replaced by the child

	   		   //finding the best in the population
	   			  BestIndex=0;
	   			  tempfit=Population[0].Fitness;
	   			  cout<<tempfit<<" is temp fit"<<endl;
	   			  for(int i=1;i<PopSize;i++)
	   			    if((MINIMIZE * Population[i].Fitness) < (MINIMIZE * tempfit))
	   			      {
	   				tempfit=Population[i].Fitness;
	   				BestIndex=i;

	   			//	cout<<"yees  "<<BestIndex<<endl;
	   			      }

	   			  // print out results after every 100 generations
	   			    if (((count%100)==0) || (tempfit <= LIMIT))
	   			   out1<<count*kids<<"    "<<tempfit<<endl;

	   			 cout<<count<<" --------"<<tempfit<<endl;

	   			 		    		 //for(int i=0;i<	NumVariable;i++)
	   			 		    			// cout<<Population[BestIndex].Chrome[i]<<" ";
	   			 		    		 //cout<<endl;
    	}

   TOC();
      //  cout<<" Run Number: "<<RUN<<endl;
  // out2<<"Best solution obtained after X function evaluations:"<<count*kids<<" "<<NumEval<<endl;
out2<<RUN<<"   ";
        for(int i=0;i<NumVariable;i++)
  	 out2<<Population[BestIndex].Chrome[i]<<"      ";
        out2<<"---->  "<< tempfit<<"     "<<count*kids<<"      "<<StopwatchTimeInSeconds()<<endl;

        //out2<<"Fitness of this best solution:"<<tempfit<<endl;

        cout<<"Best solution obtained after X function evaluations:"<<count*kids<<" "<<NumEval<<endl;

              for(int i=0;i<NumVariable;i++)
            	  cout<<Population[BestIndex].Chrome[i]<<" ";
              cout<<endl;

              cout<<"Fitness of this best solution:"<<tempfit<<" "<<StopwatchTimeInSeconds()<<endl;


 	//   PrintPopulation();

     // }//run

}
//--------------------------------------------------------------------------------------------


typedef vector<GeneticAlgorithmn> GAvector;

class Table{

	//  friend class  CoEvolution ;
    //  friend class CombinedEvolution;
	    public:

       //double   SingleSp[100];
       Layer   SingleSp ;



};

typedef vector<Table> TableVector;

class CoEvolution :  public  NeuralNetwork,   public virtual TrainingExamples,      public   GeneticAlgorithmn  ,public virtual RandomNumber
{

	// friend class  Table;
	// friend class  GeneticAlgorithmn;
	//friend class Individual;
    //friend class TrainingExamples;
       // friend class NeuralNetwork;
        //friend class  Layers;
     // friend class CombinedEvolution;
 //      protected:

public:

      int  NoSpecies;
    GAvector Species ;
   // GeneticAlgorithmn SingleGA;
//    Table  TableSp[NoSpeciesCons];

    TableVector  TableSp;
int PopSize;
    vector<bool> NotConverged;
       Sizes SpeciesSize;
       Layer   Individual;
       Layer  BestIndividual;
      double bestglobalfit;
      Data TempTable;
int TotalEval;
     int TotalSize;
     int SingleGAsize;
     double Train;
          double Test;
     int kid;

      // public:
      // CoEvolution(int Size){
//
    //	   SpeciesSize = Size;
   //    }

    CoEvolution(){

    }

       void   MainProcedure(bool bp,   int RUN, ofstream &out1, ofstream &out2, ofstream &out3, double mutation,int depth );

     void  InitializeSpecies(int popsize);
     void  EvaluateSpecies(NeuralNetwork network,TrainingExamples Sample);
     void    GetBestTable(int sp);
     void PrintSpecies();

     void   Join();
     double    ObjectiveFunc(Layer x);
     void Print();

     void   sort(int s );

     void  find_parents(int s,NeuralNetwork network,TrainingExamples Sample);

     void EvalNewPop(int pass, int s,NeuralNetwork network,TrainingExamples Sample);

     void  rep_parents(int s,NeuralNetwork network,TrainingExamples Sample);

     void   EvolveSubPopulations(int repetitions,double h, NeuralNetwork network,TrainingExamples Sample,double mutation,int depth, ofstream &out2);

       };

void    CoEvolution:: InitializeSpecies(int popsize)
{         PopSize = popsize;
	 // for( int Sp = 0; Sp < NoSpecies; Sp++){
	   //   Species.push_back(0);
	   //   }

           GAvector SpeciesP(NoSpecies);
          Species = SpeciesP;
        
	 for( int Sp = 0; Sp < NoSpecies; Sp++){
	 	     NotConverged.push_back(false);
	 	     }

	   basic_seed=0.4122;
		   RUN =2;

		   seed=basic_seed+(1.0-basic_seed)*(double)((RUN)-1)/(double)MAXRUN;
		 			    	            if(seed>1.0) printf("\n warning!!! seed number exceeds 1.0");

			    			for( int s =0; s < NoSpecies; s++){

			    			Species[s].randomize(seed);

			    			}

			//   SingleGA.NumVariable = SingleGAsize;
		//	  SingleGA.InitilisePopulation();



	 TotalSize = 0;
		for( int row = 0; row< NoSpecies ; row++)
			TotalSize+= SpeciesSize[row];

		for( int row = 0; row< TotalSize ; row++)
		        Individual.push_back(0);

	 for( int s =0; s < NoSpecies; s++){
                 
	 	Species[s].NumVariable= SpeciesSize[s];
		  Species[s].InitilisePopulation(popsize);
     }



	        TableSp.resize(NoSpecies);

             for( int row = 0; row< NoSpecies ; row++)
	            for( int col = 0; col < SpeciesSize[row]; col++)
	         	   TableSp[row].SingleSp.push_back(0);
 

}


void    CoEvolution:: PrintSpecies()
{

	 for( int s =0; s < NoSpecies; s++){
		 Species[s].PrintPopulation();
		cout<<s<<endl;
     }

}

void    CoEvolution:: GetBestTable(int CurrentSp)
{
  int Best;

		 for(int sN = 0; sN < CurrentSp ; sN++){
		    Best= Species[sN].BestIndex;

          // cout<<Best<<endl;
		  for(int s = 0; s < SpeciesSize[sN] ; s++)
		    	 TableSp[sN].SingleSp[s] = Species[sN].Population[Best].Chrome[s];
		 }

		 for(int sN = CurrentSp; sN < NoSpecies ; sN++){
			// cout<<"g"<<endl;
			Best= Species[sN].BestIndex;
			   //cout<<Best<<" ****"<<endl;
	     for(int s = 0; s < SpeciesSize[sN] ; s++)
			TableSp[sN].SingleSp[s]= Species[sN].Population[Best].Chrome[s];
				 }

}

void   CoEvolution:: Join()
{


	int index = 0;

	  for( int row = 0; row< NoSpecies ; row++){
		for( int col = 0; col < SpeciesSize[row]; col++){

			//if( ((TableSp[row].SingleSp[col]/1)!=TableSp[row].SingleSp[col]))
				//cout<<"************************************error******************"<<endl;
		  Individual[index] =  TableSp[row].SingleSp[col];
		  index++;
		}

	  }


}

void   CoEvolution:: Print()
{



	  for( int row = 0; row< NoSpecies ; row++){
		for( int col = 0; col < SpeciesSize[row]; col++){
		 cout<<TableSp[row].SingleSp[col]<<" ";   }
            cout<<endl;
	  }
            cout<<endl;

            for( int row = 0; row< TotalSize ; row++)
              cout<<Individual[row]<<" ";
            cout<<endl<<endl;
}
 
void    CoEvolution:: EvaluateSpecies(NeuralNetwork network,TrainingExamples Sample)
{

 
	 for( int SpNum =0; SpNum < NoSpecies; SpNum++){

		 GetBestTable(SpNum);

//---------make the first individual in the population the best

		 for(int i=0; i < Species[SpNum].NumVariable; i++)
		  TableSp[SpNum].SingleSp[i] = Species[SpNum].Population[0].Chrome[i];

		    Join();
		 Species[SpNum].Population[0].Fitness =  network.ForwardFitnessPass(Individual, Sample);//ObjectiveFunc(Individual);
		 TotalEval++;

		 Species[SpNum].BestFit = Species[SpNum].Population[0].Fitness;
		 Species[SpNum].BestIndex = 0;
		// cout<<"g"<<endl;
			 //------------do for the rest

		 for( int PIndex=0; PIndex< PopSize; PIndex++ ){


			 for(int i=0; i < Species[SpNum].NumVariable; i++)
	          TableSp[SpNum].SingleSp[i]  = Species[SpNum].Population[PIndex].Chrome[i];


		     Join();
             //Print();

		     Species[SpNum].Population[PIndex].Fitness = network.ForwardFitnessPass(Individual, Sample);//
		     TotalEval++;
		     //   ObjectiveFunc(Individual);

		     if ((MINIMIZE * Species[SpNum].BestFit) > (MINIMIZE * Species[SpNum].Population[PIndex].Fitness))
		       {
		    	 Species[SpNum].BestFit = Species[SpNum].Population[PIndex].Fitness;
		    	 Species[SpNum].BestIndex = PIndex;
		    	//  cout<<Species[SpNum].Population[PIndex].Fitness<<endl;
		       }

         }

			// cout<< Species[SpNum].BestIndex<<endl;
		cout<<SpNum<<" -- "<<endl;
     }







}

double   CoEvolution:: ObjectiveFunc(Layer x)
{
  int i,j,k;
  double fit, sumSCH;

  

  return(fit);
}


void CoEvolution::find_parents(int s, NeuralNetwork network,TrainingExamples Sample)   //here the parents to be replaced are added to the temporary sub-population to assess their goodness against the new solutions formed which will be the basis of whether they should be kept or not
{
  int i,j,k;
  double u,v;

  Species[s].my_family();

  for(j=0;j<family;j++)
    {

    	  Species[s].NewPop[Species[s].kids+j].Chrome = Species[s].Population[Species[s].mom[j]].Chrome;



		 GetBestTable(s);
		 for(int i=0; i < Species[s].NumVariable; i++)
		  TableSp[s].SingleSp[i] =   Species[s].NewPop[Species[s].kids+j].Chrome[i];
		    Join();

		 Species[s].NewPop[Species[s].kids+j].Fitness =  network.ForwardFitnessPass(Individual, Sample);//ObjectiveFunc(Individual);
		 TotalEval++;
    }
}


void CoEvolution::EvalNewPop(int pass, int s,NeuralNetwork network,TrainingExamples Sample)
{

	 GetBestTable(s);
	 for(int i=0; i < Species[s].NumVariable; i++)
     TableSp[s].SingleSp[i] = 	Species[s].NewPop[pass].Chrome[i];
     Join();
  //   cout<<  Species[s].NewPop[pass].Fitness << "        pop fit"<<endl;

	 Species[s].NewPop[pass].Fitness =  network.ForwardFitnessPass(Individual, Sample);// ObjectiveFunc(Individual);
	 TotalEval++;


	  //   cout<<  Species[s].NewPop[pass].Fitness << "is new pop fit"<<endl;
}
void  CoEvolution::sort(int s)

{
  int i,j, temp;
  double dbest;

  for (i=0;i<(Species[s].kids+family);i++) Species[s].list[i] = i;

  if(MINIMIZE)
    for (i=0; i<(Species[s].kids+family-1); i++)
      {
	dbest = Species[s].NewPop[Species[s].list[i]].Fitness;
	for (j=i+1; j<(Species[s].kids+family); j++)
	  {
	    if(Species[s].NewPop[Species[s].list[j]].Fitness < dbest)
	      {
		dbest = Species[s].NewPop[Species[s].list[j]].Fitness;
		temp = Species[s].list[j];
		Species[s].list[j] = Species[s].list[i];
		Species[s].list[i] = temp;
	      }
	  }
      }
  else
    for (i=0; i<(Species[s].kids+family-1); i++)
      {
	dbest = Species[s].NewPop[Species[s].list[i]].Fitness;
	for (j=i+1; j<(Species[s].kids+family); j++)
	  {
	    if(Species[s].NewPop[Species[s].list[j]].Fitness > dbest)
	      {
		dbest = Species[s].NewPop[Species[s].list[j]].Fitness;
		temp = Species[s].list[j];
		Species[s].list[j] = Species[s].list[i];
		Species[s].list[i] = temp;
	      }
	  }
      }
}


void CoEvolution::rep_parents(int s,NeuralNetwork network,TrainingExamples Sample)   //here the best (1 or 2) individuals replace the family of parents
{
  int i,j;
  for(j=0;j<family;j++)
    {

   Species[s].Population[Species[s].mom[j]].Chrome = Species[s].NewPop[Species[s].list[j]].Chrome;

    	  GetBestTable(s);

    		 for(int i=0; i < Species[s].NumVariable; i++)
    	 TableSp[s].SingleSp[i] =   Species[s].Population[Species[s].mom[j]].Chrome[i];
    	  Join();

    	  Species[s].Population[Species[s].mom[j]].Fitness =  network.ForwardFitnessPass(Individual, Sample);//ObjectiveFunc(Individual);
    	  TotalEval++;
    }
}

void CoEvolution:: EvolveSubPopulations(int repetitions,double h, NeuralNetwork network,TrainingExamples Samples, double mutation,int depth, ofstream & out1)
{
double tempfit;
		int count =0;
		int tag;
		kid = KIDS;
               int numspecies =0;

          /* for( int s =0; s < NoSpecies; s++) {
              if(NotConverged[s]==false) 
                 numspecies++;}
             
            if(numspecies == (NoSpecies-1)){
             out1<<"tag3  "<<numspecies<<endl;
             end = true;
             return;}*/
           

	for( int s =0; s < NoSpecies; s++) {
              if(NotConverged[s]==true){ 
               //  numspecies++;
               for (int r = 0; r < repetitions; r++){ 

	    		tempfit=   Species[s].Population[Species[s].BestIndex].Fitness;
	    		    Species[s].kids = KIDS;


	    	 	Species[s].RandomParents();

	 	        for(int i=0;i<	Species[s].kids;i++)
	 	   	     {
	 	   	      tag = 	Species[s].GenerateNewPCX(i, network, Samples,mutation, depth); //generate a child using PCX

	 	   	        if (tag == 0) {
                                   NotConverged[s]=false;
                                 //NewPop[pass].Chrome[k] end = true;
                               //   out1<<"tag1"<<endl;
                                   break;
                              }
	 	   	     }
	 	   	          if (tag == 0) {
                                       //   end = true;
                                       // out1<<"tag2"<<endl;
	 	   	        	NotConverged[s]=false;
	 	   	          }

	 	   	     for(int i=0;i<	Species[s].kids;i++)
	 	   	      EvalNewPop(i,s, network, Samples);


	 	      find_parents(s,network, Samples);  // form a pool from which a solution is to be
	 	   	                           //   replaced by the created child

	 	  	    Species[s].sort();          // sort the kids+parents by fitness
            //       sort(s);
 	 	      rep_parents(s,network, Samples);   // a chosen parent is replaced by the child


	 	  	   Species[s].BestIndex=0;

	 	  	         tempfit= Species[s].Population[0].Fitness;

	 	  	         for(int i=1;i<PopSize;i++)
	 	   			    if((MINIMIZE *    Species[s].Population[i].Fitness) < (MINIMIZE * tempfit))
	 	   			      {
	 	   				tempfit= Species[s].Population[i].Fitness;
	 	   			   Species[s].BestIndex=i;
	 	   			      }
                                 //if(isnan(Species[s].Population[Species[s].BestIndex].Fitness)){
                                   //NotConverged[s] = false;
                                    //end = true;
                                //out1<<"tag4"<<endl;
                                  // break;
                               //}

                                 //if(NoSpecies-numspecies <=2 ){
                              //  end = true;
                              //   out1<<"tag3  "<<numspecies<<endl;
                                   //   break;
                            //}

                                // out1<<s<<" "<<NotConverged[s]<<" "<<Species[s].Population[Species[s].BestIndex].Fitness<<endl;
	 	  	      //  cout<<s<<"  "<<NotConverged[s]<<" "<<Species[s].Population[Species[s].BestIndex].Fitness<<endl;
	 	  	 
	 	  	      	    		   //for(int i=0;i<	TotalSize;i++)
	 	  	      	    		  //	 cout<<Individual[i]<<" ";
	 	  	      	    	   // cout<<endl;


                               //  for(int i=0;i<	TotalSize;i++)
	 	  	      	 //   		  	  out1<<Individual[i]<<" ";
	 	  	      	   // 	   out1<<endl;
	    		}
              
               }//r
		   //numspecies =0;
	     	}//species
}




 




//public GeneticAlgorithmn, public RandomNumber,


class CombinedEvolution        :    public    CoEvolution // ,      public virtual  NeuralNetwork, public virtual TrainingExamples, public virtual GeneticAlgorithmn
{

 
       public:
    int TotalEval;
     int TotalSize;
   double Energy;
   int SAFuncEval;
     double Train;
          double Test;
           double Error;
       	 CoEvolution NeuronLevel;
CoEvolution ReverseNeuronLevel;
    CoEvolution WeightLevel;
 	 	 CoEvolution OneLevel;
		 CoEvolution LayorLevel;
int Cycles;
        bool Sucess;

          CombinedEvolution(){

          }

          int GetEval(){
            return TotalEval;
                  }
          int GetCycle(){
                     return Cycles;
                           }
          double GetError(){
                    return Error;
                          }

          bool GetSucess(){
                              return Sucess;
                                    }

          void   Procedure(bool bp,   double h, ofstream &out1, ofstream &out2, ofstream &out3, ofstream &console_output,double mutation,int depth );

 

};
void CombinedEvolution:: Procedure(bool bp,   double h, ofstream &out1, ofstream &out2, ofstream &out3, ofstream &console_output,double mutation,int depth )
{
	bool usememe = true;

	if(depth == 0)
	usememe = false;

	clock_t start = clock();

	int hidden =h;
	int weightsize1 = (input*hidden);
	int weightsize2 = (hidden*output);
	int contextsize = hidden*hidden;
	int biasize =   hidden + output;

	double trainpercent=0;
	double testpercent=0;
	int epoch;
	double testtree;
	char  file[15] = "NSPLearnt.txt";
	char  file2[15] = "BPLearnt.txt";

	ofstream out;
	out.open("Rnuuu.txt");

	double H = 0;
	int gene = 1;

	TrainingExamples Samples(trainfile,trainsize ,input+output, input, output);
	Samples.printData();
	double error;

	Sizes layersize;
	layersize.push_back(input);
	layersize.push_back(hidden);
	layersize.push_back(output);

	NeuralNetwork network(layersize);
	network.CreateNetwork(layersize,Samples ); // Create network once for CICC competition between all methods

	TotalEval = 0;

	//Layer BPIndividual = network.Neurons_to_chromes(); //Save initial network individual for bp
	//Layer NSPIndividual = network.Neurons_to_chromes();

	//bp = true;
	if(bp)
	{
				
	}
	else
	{

		Sucess= false;

		Cycles =0;

		// ######### Set up and evaluate neural level coevolution ########## //

		// set up the size of a species between input and hidden layer. Example. For each hidden neuron (loop through all of them),
		// create a species of size all input neurons + 1 bias neuron
		for(int n = 0; n < hidden ; n++)
		NeuronLevel.SpeciesSize.push_back(input+1);

		// set up the size of a species between hidden and output layer. Example. For each output neuron (loop through all of them),
		// create a species of size all hiddens neurons + 1 bias neuron

		for(int n = 0; n < output ; n++)
		NeuronLevel.SpeciesSize.push_back(hidden+1);

		NeuronLevel.NoSpecies =  hidden+output; // Total number of species is equal to the sum of all hidden and output neurons
		NeuronLevel.InitializeSpecies(CCPOPSIZE); // Generate random network weights in all species
		NeuronLevel.EvaluateSpecies(network, Samples); // Get fitness levels of all species

		cout<< "Hidden: " << h<< " Total Sub-pops: " <<NeuronLevel.NoSpecies<<endl;
		for( int s =0; s < NeuronLevel.NoSpecies; s++)
		NeuronLevel.NotConverged[s]=true;

		cout <<" Evaluated Neuronlevel ----------->" <<endl;

		// ############## Output chromes for neuron level ##############

		/*int p =0;
		int  m =0;
		out1 << "Neuron Chromes" << endl;

		for(int sp = 0; sp < NeuronLevel.NoSpecies  ; sp++)
		{
			for(int col = 0; col <  NeuronLevel.SpeciesSize[sp] ; col++)
			{
				out1 << NeuronLevel.Species[sp].Population[p].Chrome[col] << " ";
				m++;
			}

			out1 << "EOL" << endl;
		}*/


		// *************************************************************************************************//
		// ---------------------------------- 2-Island competition -----------------------------------------//
		// *************************************************************************************************//

		TotalEval=0;
		NeuronLevel.TotalEval=0;
		OneLevel.TotalEval=0;
		//LayerLevel.TotalEval=0;
		WeightLevel.TotalEval=0;
		Layer BestMemes(10000);
		Layer ErrorArray;

		int NeuronTempEval = 0;
		int count =10;
		double  OneLevelError = -1;
		double OneLevelTrain = -1;
		double BestNL, BestBP;

		network.ChoromesToNeurons(NeuronLevel.Individual); //Take weights from neuron level and pass to network
		//Layer BPIndividual = NeuronLevel.Individual;
		network.SaveLearnedData(layersize, file); // Save initial network weights to NSPLearnt.txt
		network.SaveLearnedData(layersize, file2); // Save initial network weights to BPLearnt.txt

		double total_epochs = 0;

		while(TotalEval<=maxgen)
		{

				double NeuronLevelError= 10000;
				double WeightLevelError= 10000;
				double NetLevelError= 10000;
				double NeuronLevelTrain =0;
				double  WeightLevelTrain =0;


				int neutral = 10;
               /* cout<< "#######################################################################" << endl;
				/// ---------------- 1. Backpropagation Algorithm island procedure ---------------------- ///

				cout<< "<--- Backpropagation Algorithm island procedure --->" << endl;
					//console_output << "<--- ackpropagation Algorithm island procedure --->" << endl;

				//Load BP individual to network and perform BP. get result and store in BPIndivdual
				BPIndividual = network.BackpropogationCICC(BPIndividual,Samples,0.1,layersize, file, true, 20, console_output);
                total_epochs+=20;

                network.ChoromesToNeurons(BPIndividual); //Take weights from neuron level and pass to network
				network.SaveLearnedData(layersize, file);
                BestBP = network.SumSquaredError(Samples,layersize); //Get network error

				cout<<"Backpropagation Island Epochs: " << total_epochs << " | Error: " << BestBP << endl;*/
               // console_output <<"Backpropagation Island Epochs: " << total_epochs << " | Error: " << BestBP << endl;

			//	double trained =  network.TestTrainingData(layersize,testfile,testsize, file, input, output);
				//cout<<trained<<" is #BPpercentage trained"<<endl;


				//		SumErrorSquared = SumSquaredError(TraineeSamples,Layersize);
//		cout<<SumErrorSquared<< " : is SumErrorSquared"<<endl;
//
//
            	//network.SaveLearnedDataSaveLearnedData(layersize, file);
//
//		double trained =  TestTrainingData(Layersize,testfile,testsize, Savefile, Layersize[0], Layersize[2]);
//
//		cout<<trained<<" is percentage trained"<<endl;




		 		/// ---------------- 2. Neural island procedure ---------------------- ///
		 		cout<< "<--- Neural Level island procedure --->" << endl;
		 		//console_output << "<--- Neural Level island procedure --->" << endl;
				//network.LoadSavedData(layersize,file); //Load nsp network weights

				for( int cycle = 0; cycle <(neutral); cycle++)
				{
					NeuronLevel.EvolveSubPopulations(1,1,  network, Samples, mutation,0, out2);
					NeuronLevel.GetBestTable(NeuronLevel.NoSpecies-1);
					NeuronLevel.Join();

					network.ChoromesToNeurons(NeuronLevel.Individual); //Take weights from neuron level and pass to network
					network.SaveLearnedData(layersize, file);

					NeuronLevelError= NeuronLevel.Species[NeuronLevel.NoSpecies-1].Population[NeuronLevel.Species[NeuronLevel.NoSpecies-1].BestIndex].Fitness;
				}


				cout<<  "Neuron Island Evals: "<<NeuronLevel.TotalEval<<" | Error: " << NeuronLevelError << endl;
				//console_output <<  "Neuron Island Evals: "<<NeuronLevel.TotalEval<<" | Error: " << NeuronLevelError << endl;

				BestNL = NeuronLevelError;


				 /// ---------------- 3. Compare Best errors from both islands ---------------------- ///

				count++;

				//**********************************************************//
				// ------------ If Neural level wins, copy to BP ---------- //
				//**********************************************************//



                // ################################################################################################//

				TotalEval =   NeuronLevel.TotalEval + total_epochs; // 20 is for 20 epochs that the BP ran for
				Train =   network.TestTrainingData(layersize,trainfile,trainsize, file, input, output );
				cout<< "<--- Overall Network Test Using Current Best Solution --->" << endl;
				out1 << "<--- Overall Network Test Using Current Best Solution --->" << endl;
				out1 << "< -- Train %: " << Train << " Total Evals: " << TotalEval <<  " Hidden: " << hidden << " -- >" <<endl;
				console_output << "<--- Overall Network Test Using Current Best Solution --->" << endl;
				console_output << "< -- Train %: " << Train << " Total Evals: " << TotalEval <<  " Hidden: " << hidden << " -- >" <<endl;
				cout << "< -- Train %: " << Train << " Total Evals: " << TotalEval <<  " Hidden: " << hidden << " -- >" <<endl;

				if(Train >=mintrain)
				{
					Test = network.TestLearnedData(layersize,testfile,testsize, file, input, output);
					Cycles=  Error;
					Error = Test;
					Sucess =true;
					break;
				}

		}//end while

		Test = network.TestLearnedData(layersize,testfile,testsize, file, input, output);
		Error = Test;

		Test = network.TestLearnedData(layersize,testfile,testsize, file, input, output);
		cout<<Test<<" was the test in general"<<endl;
		out1<<endl;
		out1<<Test<<" was the test in general"<<endl;
		out2<<h<<"   "<<Error<<"   "<<TotalEval<<"  "<<Train<<"  "<<Test<<endl;

	}
}












//---------------------------------------------------------------------------------------
int main(void)
{

	int VSize =90;

	ofstream out1;
		out1.open("Oneout1.txt");
	ofstream out2;
	     out2.open("Oneout2.txt");
	 	ofstream out3;
	 	     out3.open("Oneout3.txt");
ofstream out4;
	 	     out4.open("Oneout4.txt");

	 	     ofstream console_output;
		console_output.open("Console_Output.txt");

    FILE * pFile;
     pFile = fopen ("Output3all.txt","a");



	  for(RUN=1;RUN<MAXRUN;RUN++){
  // GeneticAlgorithmn GenAlg(VSize);
 		      // GenAlg.MainAlgorithm(RUN,out1,out2, out3);

 	 cout<<RUN<<endl;

 	     }
cout<<"--------------*************-------------**********--------------*************"<<endl;
 

 
     for(int hidden = 4; hidden < 13; hidden+=2)
	 {
    	 Sizes EvalAverage;
    	 Layer ErrorAverage;
    	Layer CycleAverage;

    	 int MeanEval=0;
    	 double MeanError=0;
    	double MeanCycle=0;

    	 int EvalSum=0;
    	 double ErrorSum=0;
    	double CycleSum=0;
    	double maxrun =20;
    	
int success =0;
    	 for(int run=1;run<=maxrun;run++){
    	  CombinedEvolution Combined;

                    Combined.Procedure(false, hidden, out1, out2, out3, console_output, 0  , 0 );
if(Combined.GetSucess()){
   success++;

   ErrorAverage.push_back(Combined.GetError());
    	  MeanError+= Combined.GetError();

    	  CycleAverage.push_back(Combined.GetCycle() );
    	      	  MeanCycle+= Combined.GetCycle();

          }


          EvalAverage.push_back(Combined.GetEval());
          MeanEval+=Combined.GetEval();

    	               }//run

    	 MeanEval=MeanEval/EvalAverage.size();
    	 MeanError=MeanError/ErrorAverage.size();
    	 MeanCycle=MeanCycle/CycleAverage.size();

    	  for(int a=0; a < EvalAverage.size();a++)
    	EvalSum +=	(EvalAverage[a]-MeanEval)*(EvalAverage[a]-MeanEval);

    	  EvalSum=EvalSum/ EvalAverage.size();
    	  EvalSum = sqrt(EvalSum);

    	  EvalSum = 1.96*(EvalSum/sqrt( EvalAverage.size()));
    	  for(int a=0; a < CycleAverage.size();a++)
    		  CycleSum +=	(CycleAverage[a]-MeanCycle)*(CycleAverage[a]-MeanCycle);

    	  CycleSum=CycleSum/ CycleAverage.size();
    	  CycleSum = sqrt(CycleSum);
    	  CycleSum = 1.96*(CycleSum/sqrt( CycleAverage.size()));
    	  for(int a=0; a < ErrorAverage.size();a++)
        ErrorSum +=	(ErrorAverage[a]-MeanError)*(ErrorAverage[a]-MeanError);

    	 ErrorSum=ErrorSum/ ErrorAverage.size();
    	ErrorSum = sqrt(ErrorSum);
        ErrorSum = 1.96*(ErrorSum/sqrt(ErrorAverage.size()) );
     //	out3<<onelevelstop<<" "<<hidden<<"  & $"<<MeanEval<<"_{\\hspace{3mm} "<<EvalSum<<"}$ & $"<<MeanError<<"_{\\hspace{3mm} "<<ErrorSum<<"}$  &  $"<<MeanCycle<<"_{\\hspace{3mm} "<<CycleSum<<"}$ & "<<success<<"  \\\\"<<endl;
	out3<< hidden <<"  "<<MeanEval<<"  "<<EvalSum<<"  "<<MeanError<<"  "<<ErrorSum<<" "<<MeanCycle<<"  "<<CycleSum<<"  "<<success<<"  "<<endl;

       out4<<hidden<<"  "<<MeanEval<< " "<<success<<"  "<<endl;


     	EvalAverage.empty();
     	ErrorAverage.empty();
     	CycleAverage.empty();
    	 }//onelevelstop

     out3<<"\\hline"<<endl;

fprintf ( pFile, " \n -----------------  \n");

    fclose (pFile);
	out1.close();
	out2.close();
	out3.close();
out4.close();


 return 0;

};





