/*
	--Competitive Island Cooperative Neuroevolution with Backpropagation --

	Framework Originally Developed by: Rohitash Chandra (2009)
	Cleaned, Documented and Modified by: Shelvin Chand (2013 - 2014)
	Extended Code and Documentation for CICN-BP: Gary Wong (2015)
*/

#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <string>
#include <vector>
#include <algorithm>
#include <fstream>
#include <ctime>
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<ctype.h>

time_t TicTime;
time_t TocTime;
using namespace::std;

//Type definitions for vectors
typedef vector<double> Layer;
typedef vector<double> Nodes;
typedef vector<double> Frame;
typedef vector<int> Sizes;
typedef vector<vector<double> > Weight;
typedef vector<vector<double> > Data;

//Constants
const int LayersNumber = 3; 	//Total number of layers - 1 Input, 1 Hidden and 1 Output
const int culturalPopSize = 20;
const int CCPOPSIZE = 200; 	//Population size
const int minhidden = 1;	//Minimum number of hidden layers
const int maxhidden = 3;	//Maximum number of hidden layers
const int depthbegin = 10;
const int depthend = 10;
const int depthinc = 5;

const double mintrain = 95;		//Minimum training percentage 
const int maxgen = 15000;		//Max number of function evaluations for all islands
const int fixedhidden = 5;
const int trainsize = 110;		//Size of training set
const int testsize = 40;		//Size of test set
const   int input = 4; 			//Input set dimensions (9 input neurons)
const int output = 2;			//Output set dimensions (1 output neuron)
char *  trainfile = "iris.csv";		//Training examples file
char  * testfile = "test.csv";		//Test examples file

const double alpha = 0.00001;
const double MaxErrorTollerance = 0.20;	//Error tolerance --> used to round up/down values
const double MomentumRate = 0;
const double Beta = 0;
int row;
int col;
int layer;
int r;
int x;
int y;

double weightdecay = 0.005;

#include "RandomNumber.h"       //random number generator
#define rosen          	//Choose the function
#define neuronlevel   	//Decomposition Method
#define EPSILON 1e-50
#define MAXFUN 15000 	//Upper bound for number of function evaluations
#define MINIMIZE 1      //Set 1 to minimize and -1 to maximize
#define LIMIT 1e-20     //Accuracy of best solution fitness desired
#define KIDS 2          //Pool size of kids to be formed (use 2,3 or 4)
#define M 1             //M+2 is the number of parents participating in xover (use 1)
#define family 2        //Number of parents to be replaced by good individuals(use 1 or 2)
#define sigma_zeta 0.1
#define sigma_eta 0.1   //Variances used in PCX (best if fixed at these values)
#define  NoSpeciesCons 300
#define NPSize KIDS + 2 //New pop size
#define RandParent M+2  //Number of parents participating in PCX
#define MAXRUN 10      	//Number of runs each with different random initial population

double d_not[CCPOPSIZE];
double seed, basic_seed;
int RUN;

/***************************************************************************************

			--1. TrainingExamples Class--		
		
This class manages training dataset. Reads from example files and stores in appropriate 
vectors

***************************************************************************************/

class TrainingExamples
{
	friend class NeuralNetwork;

	//Class variables
	protected:

	Data  InputValues;
	Data  DataSet;
	Data  OutputValues;
	char* FileName;
	int Datapoints;
	int colSize;
	int inputcolumnSize;
	int outputcolumnSize;
	int datafilesize;

	public:
	TrainingExamples()
	{
		//Constructor
	};

	TrainingExamples(char* File, int size, int length, int inputsize, int outputsize)
	{
		//Initialize functions and class variables
		inputcolumnSize = inputsize;
		outputcolumnSize = outputsize;
		datafilesize = inputsize + outputsize;
		colSize = length;
		Datapoints = size;
		FileName = File;
		InitialiseData();
	}

	void printData();
	void InitialiseData();

};

/*
 	--InitialiseData--
 	Function is used to setup the dataset, input and output vectors.
 	1.The dataset vector is initialised with the data from the file
 	2.The dataset vector is then used to initialize the output/input vectors
*/

void TrainingExamples::InitialiseData()
{
	ifstream in(FileName);
	if (!in) {
		cout << endl << "failed to open file" << endl; //File could not be read message

	}

	//Initialise DataSet from contents of training examples file
	for (int r = 0; r < Datapoints; r++)
		DataSet.push_back(vector<double>());	//Create a matrix for holding the data

	for (int row = 0; row < Datapoints; row++) {
		for (int col = 0; col < colSize; col++)
			DataSet[row].push_back(0);	//Initialize dataset with 0s
	}
	//  cout<<"printing..."<<endl;
	for (int row = 0; row < Datapoints; row++)
	for (int col = 0; col < colSize; col++)
		in >> DataSet[row][col];	 //Fill dataset matrix with values from the training examples file
	
	//Initialise input vectors
	for (int r = 0; r < Datapoints; r++)
		InputValues.push_back(vector<double>()); //Create a matrix for holding the input data from dataset

	for (int row = 0; row < Datapoints; row++)
	for (col = 0; col < inputcolumnSize; col++)
		InputValues[row].push_back(0);	//Initialise input vector with 0s

	for (int row = 0; row < Datapoints; row++)
	for (int col = 0; col < inputcolumnSize; col++)
		InputValues[row][col] = DataSet[row][col];  //Read values from the dataset vector into input vector

	//Initialise output vectors
	for (int r = 0; r < Datapoints; r++)
		OutputValues.push_back(vector<double>()); //Create a matrix for holding the output data from dataset

	for (int row = 0; row < Datapoints; row++)
	for (int col = 0; col < outputcolumnSize; col++)
		OutputValues[row].push_back(0); //Initialise output vector with 0s

	for (int row = 0; row < Datapoints; row++)
	for (int col = 0; col < outputcolumnSize; col++)
		OutputValues[row][col] = DataSet[row][col + inputcolumnSize]; //Read values from the dataset vector into output vector

	in.close();	//Close connection
}

/*
 	--printData--
 	Displays to the user the entire training set including all the input values and the expected output values
 	
*/

void TrainingExamples::printData()
{
	cout << "Printing...." << endl;
	cout<<"Entire Data Set.."<<endl;
	for (int row = 0; row < Datapoints; row++) 
	{
		for (int col = 0; col < colSize; col++)
		cout << DataSet[row][col] << " "; //Print entire dataset
		cout << row << endl;
	}

	cout<<endl<<"Input Values.."<<endl;

	for (int row = 0; row < Datapoints; row++) 
	{
		for (int col = 0; col < inputcolumnSize; col++)
		cout << InputValues[row][col] << " ";	//Print input values
		cout << endl;
	}

	cout<<endl<<"Expected Output Values.."<<endl;   

	for (int row = 0; row < Datapoints; row++)  
	{
		for (int col = 0; col < outputcolumnSize; col++)
			cout << OutputValues[row][col] << " "; //Print output values

		cout << endl;
	}
}


/***************************************************************************************

			--2. Layer Class--		
		
Layer Class is used to manage the different layers of the neural network. It manages 
the weights, neuron values, bias factor and changes in weights and bias. Each element is 
a vector or vector containing other vectors forming a multi-dimensional matrix. This allows 
for managing of corresponding weights and values.

***************************************************************************************/

class Layers
{
	friend class NeuralNetwork;

protected:
	//Class variables
        Weight Weights ;//Neural network weights 
        Weight WeightChange;//Change in weight after each iteration
        Weight H;
        Layer Outputlayer;
        Layer Bias;//Keeping track of the bias factor in the network
        Layer B;
        Layer Gates;
        Layer BiasChange;//Keeping track of change in Bias factor
        Layer Error;//Error after each iteration

public:
	Layers()
	{

	}

};

/***************************************************************************************

			--3. NeuralNetwork Class--		
		
This class is used to represent the neural network structure and its functionality. 
It makes use of the layer class to structure the network

***************************************************************************************/

class NeuralNetwork
{
	friend class GeneticAlgorithmn;
	friend class CoEvolution;
protected:
	Layers nLayer[LayersNumber];
	double Heuristic;
	int StringSize;
	Layer ChromeNeuron;
	int NumEval;
	Data Output;
	Sizes layersize;

public:
	NeuralNetwork()
	{


	}
	NeuralNetwork(Sizes layer)
	{
		//Constructor override
		layersize = layer;

		StringSize = (layer[0] * layer[1]) + (layer[1] * layer[2]) + (layer[1] + layer[2]);
	}

	//Class functions
	void PlaceHeuristic(double H)
	{
		Heuristic = H;
	}
	double Random();
	Layer BPchrome(){ return ChromeNeuron; }
	double Sigmoid(double ForwardOutput);
	void CreateNetwork(Sizes Layersize, TrainingExamples TraineeSamples);
	double  ForwardPassP(TrainingExamples TraineeSamples, int patternNum, Sizes Layersize);
	void ForwardPass(TrainingExamples TraineeSamp, int patternNum, Sizes Layersize);
	void BackwardPass(TrainingExamples TraineeSamp, double LearningRate, int patternNum, Sizes Layersize);
	void PrintWeights(Sizes Layersize);// print  all weights
	bool ErrorTolerance(TrainingExamples TraineeSamples, Sizes Layersize, double TrainStopPercent);
	bool ErrorToleranceHalf(TrainingExamples TraineeSamples, Sizes Layersize, double TrainStopPercent);
	double SumSquaredError(TrainingExamples TraineeSamples, Sizes Layersize);
	int BackPropogation(TrainingExamples TraineeSamples, double LearningRate, Sizes Layersize, char* Savefile, bool load);
	Layer BackpropogationCICC(Layer NeuronChrome, TrainingExamples TraineeSamples, double LearningRate, Sizes Layersize, char* Savefile, bool load, int EpochMax, int Island);
	void SaveLearnedData(Sizes Layersize, char* filename);
	double Rate(TrainingExamples TraineeSamples, Sizes Layersize);
	void LoadSavedData(Sizes Layersize, char* filename);
	double TestLearnedData(Sizes Layersize, char* filename, int  size, char* load, int inputsize, int outputsize);
	double  BP(Layer NeuronChrome, TrainingExamples Test, int generations);
	Layer  Neurons_to_chromes(int Island);
	double  CountLearningData(TrainingExamples TraineeSamples, int temp, Sizes Layersize);
	double  TestTrainingData(Sizes Layersize, char* filename, int  size, char* load, int inputsize, int outputsize);
	double CountTestingData(TrainingExamples TraineeSamples, int temp, Sizes Layersize);
	double LearningRate(TrainingExamples TraineeSamples, Sizes Layersize, int pattern);
	bool CheckOutput(TrainingExamples TraineeSamples, int pattern, Sizes Layersize);
	double GenerateTreeData(char* TestingFile, int testsize, Sizes Layersize, char* filename, char* load);
	void  ChoromesToNeurons(Layer NeuronChrome, int Island);
	double ForwardFitnessPass(Layer NeuronChrome, TrainingExamples Test, int Island);
};

/*
	--Random--
	Is used to generate random numbers which are used to initialize weights and neurons when the network is created
*/

double NeuralNetwork::Random()
{
	int chance;
	double randomWeight;
	double NegativeWeight;
	chance = rand() % 2; //Randomise between positive and negative 

	if (chance == 0){
		randomWeight = rand() % 100;
		return randomWeight*0.005; //Assign positive weight
	}

	if (chance == 1){
		NegativeWeight = rand() % 100;
		return NegativeWeight*-0.005; //Assign negative weight
	}
}

/*
	--Sigmoid--
	Function to convert weighted_sum into a value between  -1 and 1
*/


double NeuralNetwork::Sigmoid(double ForwardOutput)
{
	double ActualOutput;
	ActualOutput = (1.0 / (1.0 + exp(-1.0 * (ForwardOutput))));

	return  ActualOutput;
}

/*
	--CreateNetwork--
	This function is responsible for setting the overall network structure and initializing the structure with random weights, random bias factors and 0s for each neuron value.
*/

void NeuralNetwork::CreateNetwork(Sizes Layersize, TrainingExamples TraineeSamples)
{
	//Create network and initialize the weights

	int end = Layersize.size() - 1;

	for (layer = 0; layer < Layersize.size() - 1; layer++) //Go through each layer
	{
		for(int  r=0; r < Layersize[layer]; r++)//Go through the number of connections betwneen layers i.e the weights for the connections between layers
		nLayer[layer].Weights.push_back(vector<double> ()); //Each layer will have matrix of vectors representing  the weights 

		for( int row = 0; row< Layersize[layer] ; row++)
		for( int col = 0; col < Layersize[layer+1]; col++)
		nLayer[layer].Weights[row].push_back(Random());	//Initialize weights to random values for all layers

		for(int  r=0; r < Layersize[layer]; r++)              //The weight change vector will have a vecctor(of type double) inside it for each of the elements
		nLayer[layer].WeightChange.push_back(vector<double> ()); //This will be used to keep track of the change in weights after each iteration
		//again this depends on number of connections(weights) we have between layers

		for( int row = 0; row < Layersize[layer] ; row ++)
		for( col = 0; col < Layersize[layer+1]; col++)
		nLayer[layer].WeightChange[row ].push_back(0);//Initialize all the elements in weighchange matrix with 0 as initially we have no change

		for( int r=0; r < Layersize[layer]; r++)
		nLayer[layer].H.push_back(vector<double> ());//Create matrix

		for(int  row = 0; row < Layersize[layer] ; row ++)
		for( int col = 0; col < Layersize[layer+1]; col++)
		nLayer[layer].H[row ].push_back(0);//Initialize all the elements in H with 0 for each layer
	}

	for( layer=0; layer < Layersize.size(); layer++)
	{
		for( row = 0; row < Layersize[layer] ; row ++)
		nLayer[layer].Outputlayer.push_back(0);//Initialize neurons of each layer with 0s

		for( row = 0; row < Layersize[layer] ; row ++)
		nLayer[layer ].Bias.push_back(Random());//The bias for each each layer and connection will be a random value

		for( row = 0; row < Layersize[layer] ; row ++)
		nLayer[layer ].Gates.push_back(0);//Initialize gates vector with 0s


		for( row = 0; row < Layersize[layer] ; row ++)
		nLayer[layer ].B.push_back(0);//Initialize with 0s

		for( row = 0; row < Layersize[layer] ; row ++)//For each connection we will also keep track of change in bias factor
		nLayer[layer ].BiasChange.push_back(0);//Initially it will be all 0

		for( row = 0; row < Layersize[layer] ; row ++)
		nLayer[layer ].Error.push_back(0);// Intialize error vector for each layer with 0s
	}

	for( r=0; r < TraineeSamples.Datapoints; r++)
	Output.push_back(vector<double> ());//The output vector will have nested vectors(of type double) inside it forming a matrix..depending on the number of training examples

	for( row = 0; row< TraineeSamples.Datapoints ; row++)
	for( col = 0; col < Layersize[end]; col++)
	Output[row].push_back(0);// Intialize all the rows in the output vector with 0s
	for( row = 0; row < StringSize ; row ++)
	ChromeNeuron.push_back(0); //Intialise the ChromeNeuron vector with 0s
}

/*  --Forward Pass--
	1. Feed the network values from the training set through the input layer
	2. Apply the given weights to the input values for each layer of the network
	3. Apply the bias factor to the computed weighted sum
	4. Convert the resulting values using the sigmoid function to a value between -1 and 1
	3. Once the final layer is reached output the computed values from the network.
*/

void NeuralNetwork::ForwardPass(TrainingExamples TraineeSamples, int patternNum, Sizes Layersize)
{
	//Declaring essential variables
	double WeightedSum = 0;
	double ForwardOutput;//To hold output value between -1 and 1
	int end = Layersize.size() - 1; //Know the last layer

	for(int row = 0; row < Layersize[0] ; row ++)
	nLayer[0].Outputlayer[row] = TraineeSamples.InputValues[patternNum][row];//Initializing first layer with values from training examples

	for(int layer=0; layer < Layersize.size()-1; layer++)
	{//Go layer by layer calculating forward pass...i.e multiplying neuron values by the connection weights

		for(int y = 0; y< Layersize[layer+1]; y++) 
		{
			for(int x = 0; x< Layersize[layer] ; x++)
			{
				WeightedSum += (nLayer[layer].Outputlayer[x] * nLayer[layer].Weights[x][y]);//Multiplying weights from the weights matrix by the value in the corresponding neuron  to determine value for neuron in next layer

				ForwardOutput = WeightedSum - nLayer[layer+1].Bias[y]; //Subtracting the bias weight from the weighted sum giving the new value for the neuron in the next layer to which these weights are connected
			}

			nLayer[layer+1].Outputlayer[y] = Sigmoid(ForwardOutput);//Convert the weighted sum to a value between 1 and -1 which will be the new value in the neuron

			WeightedSum = 0;//Reset weighted sum to 0 for next neuron
		}
		WeightedSum = 0;//Reset weighted sum to 0 for next layer
	}//End layer

	//--------------------------------------------
	for(int output= 0; output < Layersize[end] ; output ++)
	{
		Output[patternNum][output] = nLayer[end].Outputlayer[output];//Setting the values for the final output in the last layer.

	}


}

/*  --Backward Pass--
	Go backwards in the network trying to adjust weights in order to reach the desired output.
	1. Find out the error gradient for the output layer
	2. Find out the error gradient for the hidden layers
	3. Compute the change in weight for each layer
	4. Update the weights
*/

void NeuralNetwork::BackwardPass(TrainingExamples TraineeSamp, double LearningRate, int patternNum, Sizes Layersize)
{
	int end = Layersize.size() - 1;//Know the end layer
	double temp = 0;

	//Compute error gradient for output neurons
	for (int output = 0; output < Layersize[end]; output++) 
	{
		nLayer[end].Error[output] = (Output[patternNum][output] * (1 - Output[patternNum][output]))*(TraineeSamp.OutputValues[patternNum][output] - Output[patternNum][output]);
	}
	
	for(int layer = Layersize.size()-2; layer != 0; layer--){

		for( x = 0; x< Layersize[layer] ; x++){  //Inner layer
			for( y = 0; y< Layersize[layer+1]; y++) { //Outer layer
				temp += ( nLayer[layer+1].Error[y] * nLayer[layer].Weights[x][y]);
            }
			nLayer[layer].Error[x] = nLayer[layer].Outputlayer[x] * (1-nLayer[layer].Outputlayer[x]) * temp; //Compute error gradient for each hidden neuron
 
			temp = 0.0;//Reset temp for the next neuron
		}
		temp = 0.0; //Reset temp for the next layer

	}
	double tmp;
  	int layer =0;
	for( layer = Layersize.size()-2; layer != -1; layer--)
	{//Go through all layers
 
		for( x = 0; x< Layersize[layer] ; x++)
		{  //Inner layer
			for( y = 0; y< Layersize[layer+1]; y++) 
			{ //Outer layer
				tmp = (( LearningRate * nLayer[layer+1].Error[y] * nLayer[layer].Outputlayer[x])  );// Calculate change in weight i.e error correction
				nLayer[layer].Weights[x][y] += ( tmp  -  ( alpha * tmp) ) ;//Update weight
    
            		}
		}
	}

	double tmp1;

	for( layer = Layersize.size()-1; layer != 0; layer--)
	{//Go through all layers

		for( y = 0; y< Layersize[layer]; y++)
		{
			tmp1 = (( -1 * LearningRate * nLayer[layer].Error[y])  );//Calculate change in bias
			nLayer[layer].Bias[y] +=  ( tmp1 - (alpha * tmp1))  ;//Updated bias of layer

		}
	}

}

/*
	--ErrorTolerance--
Go through all the calculated output and if the output falls within a certain range then round up or round down the value to get a whole number such as 1 or 0.
This means that the output falls within an acceptable range to be classified under a specific category
*/

bool NeuralNetwork::ErrorTolerance(TrainingExamples TraineeSamples, Sizes Layersize, double TrainStopPercent)
{
		//Declare essential variables
	double count = 0;
	int total = TraineeSamples.Datapoints;
	double accepted = total;
	double desiredoutput;
	double actualoutput;
	double Error;
	int end = Layersize.size() - 1;

	//Go through all training samples
	for(int pattern = 0; pattern< TraineeSamples.Datapoints; pattern++)
	{

		Layer Desired;
		Layer Actual;

		for(int i = 0; i <  Layersize[end] ;i++)
			Desired.push_back(0);//Initialize vector for desired output with 0s
		for(int j = 0; j <  Layersize[end] ;j++)
			Actual.push_back(0);//Initialize vector for actual output with 0s

		for(int output = 0; output < Layersize[end]; output++) 
		{
			desiredoutput = TraineeSamples.OutputValues[pattern][output];
			actualoutput = Output[pattern][output];

			Desired[output] = desiredoutput;
			//If output falls within a certain range you can round it up or down for classification
			if((actualoutput >= 0)&&(actualoutput <= 0.2))
				actualoutput = 0;//Round down
			else if((actualoutput <= 1)&&(actualoutput >= 0.8))
				actualoutput = 1;//Round up

			Actual[output] =  actualoutput;
		}
			int confirm = 0;

		for(int b = 0; b <  Layersize[end] ;b++)
		{
			if(Desired[b]== Actual[b] )
			confirm++;//There is match so move on to next neuron

			if(confirm == Layersize[end])
			count++;//If an instance is correctly predicted meaning all the values of the output layer neurons match the desired output
			confirm = 0;//reset for next layer

		}
	}

	if(count ==accepted)
	return false;

return true;
}

bool NeuralNetwork::ErrorToleranceHalf(TrainingExamples TraineeSamples, Sizes Layersize, double TrainStopPercent)
{
	double count = 0;
	int total = TraineeSamples.Datapoints;
	double accepted = total;

	double Error;
	int end = Layersize.size() - 1;

	for (int pattern = 0; pattern < TraineeSamples.Datapoints; pattern++){
		for (int output = 0; output < Layersize[end]; output++) {

			Error = TraineeSamples.OutputValues[pattern][output] - Output[pattern][output];

			if (Error < 0)
				Error = Error * -1;//absolute value of error



			if (Error <= 0.499)
				count++;

		}//for

	}//for

	double TrainPercent = 0;

	TrainPercent = (count / accepted) * 100;

	if (TrainPercent >= TrainStopPercent)
		return false;

	cout << count << "  is half the count out of " << accepted << " ::" << TrainPercent << "  percent" << endl;

	return true;
}

/*
	--SumSquardedError--
	1.Calculated error 
	2.Square the error
	3.Add to cumulative overall error and repeat steps 1 to 3 for all output values
	4. sum/(no. of training samples * no. of neurons in output layer)) and return resulT
	
*/

double NeuralNetwork::SumSquaredError(TrainingExamples TraineeSamples, Sizes Layersize)
{
	int end = Layersize.size() - 1;
	double Sum = 0;
	double Error = 0;
	double ErrorSquared = 0;
	for (int pattern = 0; pattern < TraineeSamples.Datapoints; pattern++){
		for (int output = 0; output < Layersize[end]; output++) {
			Error = TraineeSamples.OutputValues[pattern][output] - Output[pattern][output];
			
			ErrorSquared += (Error * Error); //Square the error
		}

		Sum += (ErrorSquared); //Add to cumulative error
		ErrorSquared = 0; //Set error squared variable to 0 for next layer
		
	}
	return Sum / TraineeSamples.Datapoints*Layersize[end];
}

/*
	--Rate--
	1. Calculate Error for each output
	2. Square the Error
	3. Add error to cumulative error and repeat steps 1 to 3 for all output values
	4. Divide the sum/(no. of training samples * no. of neurons in output layer) and return results
*/

double NeuralNetwork::Rate(TrainingExamples TraineeSamples, Sizes Layersize)
{
	//Variable declaration
	int end = Layersize.size() - 1;
	double Sum = 0;
	double Error=0;
	double ErrorSquared=0;
	double rate = 0;

	for(int pattern = 0; pattern< TraineeSamples.Datapoints ; pattern++)
	{
		for(int output = 0; output < Layersize[end]; output++) 
		{
			Error = TraineeSamples.OutputValues[pattern][output] - Output[pattern][output];//Calculate difference between expected and actual output
			ErrorSquared += (Error * Error);//square the error
		}

		Sum += (ErrorSquared);//add to cumulative error
		ErrorSquared=0;//reset to 0 for next training sample
	}

	rate =  Sum / (TraineeSamples.Datapoints *(Layersize[end]));//divide sum_error_squared by (no. of training samples * no. of neurons in output layer)
	return rate;
}

/*
	--PrintWeights--

	Output the weights, bias factor, error term and neuron values currently in the network
*/

void NeuralNetwork::PrintWeights(Sizes Layersize)
{
	int end = Layersize.size() - 1;

	for(int layer=0; layer < Layersize.size()-1; layer++)
	{

		cout<<layer<<"  Weights::"<<endl<<endl;
	
		for(int row  = 0; row <Layersize[layer] ; row ++)
		{
			for(int col = 0; col < Layersize[layer+1]; col++)
			cout<<nLayer[layer].Weights[row ][col]<<" "; //Output all values from the weights matrix for all layers
			cout<<endl;
		}
		cout<<endl<<layer<<"  WeightsChange::"<<endl<<endl;

		for( int row  = 0; row <Layersize[layer] ; row ++)
		{
			for( int col = 0; col < Layersize[layer+1]; col++)
			cout<<nLayer[layer].WeightChange[row ][col]<<" ";//Output all values from the weightchange matrix for all layers
			cout<<endl;
		}

		cout<<"--------------"<<endl;
	}

	for(int layer=0; layer < Layersize.size() ; layer++){
	cout<<endl<<layer<<"  Outputlayer::"<<endl<<endl;//Output values from outputlayer
	for( int row = 0; row < Layersize[layer] ; row ++)
	cout<<nLayer[layer].Outputlayer[row] <<" ";

	cout<<endl<<layer<<"  Bias::"<<endl<<endl;
	for( int row = 0; row < Layersize[layer] ; row ++)
	cout<<nLayer[layer].Bias[row] <<" ";//Output values from the Bias Matrix for each layer

	cout<<endl<<layer<<"  Error::"<<endl<<endl;
	for(int  row = 0; row < Layersize[layer] ; row ++)
	cout<<nLayer[layer].Error[row] <<" "; //Output values from the error matrix for each layer
	cout<<"----------------"<<endl;

	}
}

/*
	--SaveLearnedData--
	Save the current network weights and bias values results to file
*/

void NeuralNetwork::SaveLearnedData(Sizes Layersize, char* filename)
{

	ofstream out;
	out.open(filename);
	if (!out) {
		cout << endl << "failed to save file" << endl;
		return;
	}

	for (int layer = 0; layer < Layersize.size() - 1; layer++)
	{//Ouput weights
		for (int row = 0; row < Layersize[layer]; row++){
			for (int col = 0; col < Layersize[layer + 1]; col++)
				out << nLayer[layer].Weights[row][col] << " ";
			out << endl;
		}
		out << endl;
	}

	// Output bias.
	for (int layer = 1; layer < Layersize.size(); layer++){
		for (int y = 0; y < Layersize[layer]; y++) {
			out << nLayer[layer].Bias[y] << "  ";
			out << endl << endl;
		}
		out << endl;
	}

	out.close(); //Data saved--close connection

	return;
}

/*
	--LoadSavedData--
	Load the network weights and bias values which were able to achieve optimal results from file
*/

void NeuralNetwork::LoadSavedData(Sizes Layersize,char* filename)//Load saved data from file
{
 	ifstream in(filename);
    if(!in) {
		cout << endl << "failed to save file" << endl;//Error reading from file
		return;
    }
    
	for(int layer=0; layer < Layersize.size()-1; layer++)//read weights
		for(int row  = 0; row <Layersize[layer] ; row ++)
			for(int col = 0; col < Layersize[layer+1]; col++)
				in>>nLayer[layer].Weights[row ][col];


	for( int layer=1; layer < Layersize.size(); layer++)//Read bias
		for(int y = 0 ; y < Layersize[layer]; y++)
			in>>	nLayer[layer].Bias[y] ;

	in.close();
	cout << endl << "data loaded for testing" << endl; //Data read...close connection 
	return;
 }

/*
	--CountTestingData--
	Count no. of correctly predicted instances when working with the testing data
*/

double NeuralNetwork::CountTestingData(TrainingExamples TraineeSamples,int temp,Sizes Layersize)
{
	//Variable declaration
	double count = 0;
	int total = TraineeSamples.Datapoints;
	double accepted =  temp * 1;
	double desiredoutput;
	double actualoutput;
	double Error;
	int end = Layersize.size() - 1;

	for(int pattern = 0; pattern< temp; pattern++)
	{
		//Variable declaration
		Layer Desired;//To hold desired output from dataset
		Layer Actual;//To hold actual calculated output

		for(int i = 0; i <  Layersize[end] ;i++)
		Desired.push_back(0);//Initiliaze with 0s
		for(int j = 0; j <  Layersize[end] ;j++)
		Actual.push_back(0);//Intialize with 0s

		for(int output = 0; output < Layersize[end]; output++) 
		{//Go through all output neurons
			desiredoutput = TraineeSamples.OutputValues[pattern][output];//get desired output values
			actualoutput = Output[pattern][output];//Get calculated output value

			Desired[output] = desiredoutput;//Assign to desired output vector

			if((actualoutput >= 0)&&(actualoutput <= 0.5))
			actualoutput = 0;//If its between 0-0.5 then round it down to 0

			else if((actualoutput <= 1)&&(actualoutput >= 0.5))
			actualoutput = 1;//It its between 1 and 0.5 then round it up to 1

			Actual[output] =  actualoutput;//Store new actual output value

		}

	int confirm = 0;

	for(int b = 0; b <  Layersize[end] ;b++)
	{
		if(Desired[b]== Actual[b] )	//Check if the actual and desired output match i.e if the prediction/classification was correct
		confirm++;
	}

	if(confirm == Layersize[end])
		count++;//If an instance is correctly predicted meaning all the values of the output layer neurons match the desired output then increase count

		confirm = 0;//Reset for next set

	}

	return count;//Return count of correctly predicted instances
}

/*
	--CountLearningData--
	Get Accuracy for no. of correctly predicted instances when testing with training set
*/

double NeuralNetwork::CountLearningData(TrainingExamples TraineeSamples, int temp, Sizes Layersize)
{
		//Variable declaration
    double count = 0;
    int total = TraineeSamples.Datapoints;
    double accepted =  temp * 1;
    double desiredoutput;
    double actualoutput;
    double Error;
    int end = Layersize.size() - 1;
    double error_tolerance = 0.2;

    for(int pattern = 0; pattern< temp; pattern++){

		Layer Desired;//To hold desired output from dataset
		Layer Actual;//To hold calculated output values

		for(int i = 0; i <  Layersize[end] ;i++)
			Desired.push_back(0);//Initialize with 0s
		for(int j = 0; j <  Layersize[end] ;j++)
			Actual.push_back(0);//Initialize with 0s



		for(int output = 0; output < Layersize[end]; output++) {
			desiredoutput = TraineeSamples.OutputValues[pattern][output];//Get desired output values from training set
			actualoutput = Output[pattern][output];//Get actual output values from calculated set

			Desired[output] = desiredoutput;

			//Gepending on the error tolerance threshold-->round up/down the actual output values
			if((actualoutput >= 0)&&(actualoutput <= (0+error_tolerance)))
				actualoutput = 0;

			else if((actualoutput <= 1)&&(actualoutput >= (1-error_tolerance)))
				actualoutput = 1;

			Actual[output] =  actualoutput;//Set new actual output values

		}
     
		int confirm = 0;
		
		for(int b = 0; b <  Layersize[end] ;b++){
			if(Desired[b]== Actual[b] )
				confirm++;//Match
        }
		
		if(confirm == Layersize[end])
			count++;//If an instance is correctly predicted meaning all the values of the output layer neurons match the desired output then increase count

		confirm = 0;

    }

	return count;


}

/*
	--CheckOutput--
	To see if actual and desired output values match
*/

bool NeuralNetwork::CheckOutput(TrainingExamples TraineeSamples, int pattern, Sizes Layersize)
{

	//variable declaration
	int end = Layersize.size() - 1;//know last layer
	double desiredoutput;
	double actualoutput;
	Layer Desired; //to hold desired output from dataset
	Layer Actual; //to hold actual calculated output

	for(int i = 0; i <  Layersize[end] ;i++)
		Desired.push_back(0);//initialize with 0s
	for(int j = 0; j <  Layersize[end] ;j++)
		Actual.push_back(0);//initialize with 0s

	int count = 0;

	for(int output = 0; output < Layersize[end]; output++) 
	{//check for all neurons in output layer
		desiredoutput = TraineeSamples.OutputValues[pattern][output];//get desired output values from training set
		actualoutput = Output[pattern][output];//get actual calculated output
		Desired[output] = desiredoutput;
		cout<< "desired : "<<desiredoutput<<"      "<<actualoutput<<endl;


		//round up or round down---if actual output is between 0-0.5[round down] else if it is between 0.5-1 then round down
		if((actualoutput >= 0)&&(actualoutput <= 0.5))
		actualoutput = 0;

		else if((actualoutput <= 1)&&(actualoutput >= 0.5))
		actualoutput = 1;

		Actual[output] =  actualoutput;//new actual output value
	}

	cout<<"---------------------"<<endl;

	for(int b = 0; b <  Layersize[end] ;b++)
	{
		if(Desired[b]!= Actual[b] )//if the actual and intended output do not match then return false
		return false;
	}

	return true;
}

/*
	--TestTrainingData--
	Test the trained network with testing data
*/     

double NeuralNetwork::TestTrainingData(Sizes Layersize, char* filename, int  size, char* load, int inputsize, int outputsize)
{
	bool valid;
	double count = 1;
	double total;
	double accuracy;

	TrainingExamples Test(filename, size, inputsize + outputsize, inputsize, outputsize);
	//  Test.printData();
	total = Test.InputValues.size(); //how many samples to test?

	CreateNetwork(Layersize, Test);

	LoadSavedData(Layersize, load);

	// PrintWeights(Layersize);

	for (int pattern = 0; pattern < total; pattern++){

		ForwardPass(Test, pattern, Layersize);
	}

	count = CountLearningData(Test, size, Layersize);

	accuracy = (count / total) * 100;
	cout << "The sucessful count is " << count << " out of " << total << endl;
	cout << "The accuracy of test is: " << accuracy << " %" << endl;
	return accuracy;

}

/*
	--TestLearnedData--
	Test the network using the training data
*/

double NeuralNetwork::TestLearnedData(Sizes Layersize, char* filename, int  size, char* load, int inputsize, int outputsize)
{
	//variable declaration
	bool valid;
	double count = 1;
	double total;
	double accuracy;
	//get testing data set
	TrainingExamples Test(filename,size,inputsize+outputsize, inputsize, outputsize);

	total = Test.InputValues.size(); //how many samples to test?
	//initialize network
	CreateNetwork(Layersize,Test);
	//load saved network data
	LoadSavedData(Layersize,load);
	//do forward pass and calculate output
	for(int pattern = 0;pattern < total ;pattern++)
	{
		ForwardPass(Test,pattern,Layersize);
	}
	//get number of correctly predicted instances
	count = CountTestingData(Test,size,Layersize);

	accuracy = (count/total)* 100; //get accuracy percentage
	cout<<"The sucessful count is "<<count<<" out of "<<total<<endl;
	cout<<"The accuracy of test is: " <<accuracy<<" %"<<endl;
	return accuracy;


}

double NeuralNetwork::ForwardPassP(TrainingExamples TraineeSamples, int patternNum, Sizes Layersize)
{
	
	double WeightedSum = 0;
	double ForwardOutput;

	int end = Layersize.size() - 1;

	for (int row = 0; row < Layersize[0]; row++)
		nLayer[0].Outputlayer[row] = TraineeSamples.InputValues[patternNum][row];

	for (int layer = 0; layer < Layersize.size() - 1; layer++)
	{

		for (int y = 0; y < Layersize[layer + 1]; y++) 
		{
				for (int x = 0; x < Layersize[layer]; x++)
				{
					WeightedSum += (nLayer[layer].Outputlayer[x] * nLayer[layer].Weights[x][y]);
					ForwardOutput = WeightedSum - nLayer[layer + 1].Bias[y];
				}

			nLayer[layer + 1].Outputlayer[y] = Sigmoid(ForwardOutput);
			WeightedSum = 0;
		}
		WeightedSum = 0;
	}//end layer

	//--------------------------------------------
	for (int output = 0; output < Layersize[end]; output++)
	Output[patternNum][output] = nLayer[end].Outputlayer[output];


	double temp = 0;
	//----------------------------------------

	double tmpo = 0;
	double Rules = 0;
	double NoRules = 0;
	double WeightedError = 0;

	double B = 0;

	for (int layer = 0; layer < Layersize.size() - 1; layer++)
	{
		for (y = 0; y < Layersize[layer + 1]; y++) 
		{
			for (x = 0; x < Layersize[layer]; x++)
			{

				if (nLayer[layer].H[x][y] == 1)
				{
					Rules = (nLayer[layer].Outputlayer[x] * 1);
				}


				if (nLayer[layer + 1].Gates[y] == 1)
					B = 1;

				WeightedError = (nLayer[layer].Error[x] * nLayer[layer].Weights[x][y]);
				nLayer[layer + 1].Error[y] = (nLayer[layer + 1].Outputlayer[y] * (1 - nLayer[layer + 1].Outputlayer[y]) *((Rules + WeightedError) - B));
			}
		}

	}

	double Herror;

	for (int output = 0; output < Layersize[end]; output++) 
	{
		Herror = (-1 * (TraineeSamples.OutputValues[patternNum][0] - Output[patternNum][0]));
	}
	return Herror;
}

/*
	--BackpropogationCICC--

	Backpropagation function that takes in a network individual as parameter which is passed to network to be refined.
	1. Pass Individual to network using previous winner decomposition method
	2. Loop till a max Epoch (Island evolution time)
		2.1 Perform Backpropagation
	3. Return refined individual

*/

Layer NeuralNetwork::BackpropogationCICC(Layer ind, TrainingExamples TraineeSamples, double LearningRate, Sizes Layersize, char* Savefile, bool load, int EpochMax, int Island)
{

	/// ---------------- Backpropagation Algorithm island procedure ---------------------- ///
	double SumErrorSquared;

	ChoromesToNeurons(ind, Island); //Populate network with individual

	int Epoch = 0; //Keeps track of epoch iteration count
	bool Learn = true; //Loop end variable

	while (Learn == true)
	{
		for (int pattern = 0; pattern < TraineeSamples.InputValues.size(); pattern++)
		{//For all training examples perform forward and backward pass.
			ForwardPass(TraineeSamples, pattern, Layersize);
			BackwardPass(TraineeSamples, 0.1, pattern, Layersize); //Backward pass with learning rate set to 0.1
		}

		Epoch++;;

		SumErrorSquared = SumSquaredError(TraineeSamples, Layersize); //Get current network error
		cout << SumErrorSquared << " : is SumErrorSquared" << endl;
		
		if (Epoch == EpochMax)
			Learn = false;


	}

	return  Neurons_to_chromes(Island); //Return refined individual
}

/*
	--BackPropagation--
	Main algorithm for neural network. 
	1. Do forward pass to calculate output
	2. Do backward pass to adjust weights in order to reach desired output
	3. Calculate error/difference between actual and desired output
	4. Repeat 1 and 2 until desired accuracy is reached
*/
int NeuralNetwork::BackPropogation(  TrainingExamples TraineeSamples, double LearningRate,Sizes Layersize, char * Savefile, bool load)
{
    //variable declaration
	double SumErrorSquared;
	int Id = 0;
	int Epoch = 0;
	bool Learn = true;
	//initialize network
    CreateNetwork(Layersize,TraineeSamples );
	
	//while learning not finished
    while( Learn == true){
		//for all training data
		for(int pattern = 0; pattern < TraineeSamples.InputValues.size(); pattern++)
		{
			ForwardPass( TraineeSamples,pattern,Layersize); //forward pass through the network to calculate output values

			BackwardPass(TraineeSamples,LearningRate,pattern,Layersize);//do backward pass to update the weights

		}
		//increase number of iterations
		Epoch++;
		cout<<Epoch<< " : is Epoch    *********************    "<<endl;
		//error calculation
		SumErrorSquared = SumSquaredError(TraineeSamples,Layersize);
		cout<<SumErrorSquared<< " : is SumErrorSquared"<<endl;
		//save network weights
		SaveLearnedData(Layersize, Savefile);
		double trained = 0;
		//train for 98% accuracy
		if(trained>=98){
			Learn = false;

        }
		//or 4000 iterations
		if(Epoch ==  4000  )
			Learn = false;
	}
	//return number of iterations
	return Epoch;
}

/*
	--Neurons_to_chromes--
	Convert neurons to chromes which can be processed by the genetic algorithm
	Receives Island parameter which specifies which decomposition method to follow when
	extracting current network individual
*/

Layer NeuralNetwork::Neurons_to_chromes(int Island)
{
	int gene = 0;
	Layer NeuronChrome(StringSize);

	int layer = 0;

	if (Island == 1) //Neuron level
	{
		for (int neu = 0; neu < layersize[1]; neu++)
		{

			for (int row = 0; row < layersize[layer]; row++)
			{//Convert weights to chromes for hidden layer
				NeuronChrome[gene] = nLayer[layer].Weights[row][neu];
				gene++;
			}

			//Convert bias to chromes for hidden layer
			NeuronChrome[gene] = nLayer[layer + 1].Bias[neu]; 
			gene++;
		}

		layer = 1;

		for (int neu = 0; neu < layersize[2]; neu++)
		{

			for (int row = 0; row < layersize[layer]; row++)
			{//Convert weights to chromes for output layer
				NeuronChrome[gene] = nLayer[layer].Weights[row][neu];
				gene++;
			}

			//Convert bias to chromes for output layer
			NeuronChrome[gene] = nLayer[layer + 1].Bias[neu];
			gene++;
		}

	}

	gene = 0;

	if (Island == 2) //Network Level (Backpropagation)
	{

		for (int layer = 1; layer < layersize.size(); layer++) // For all 3 layers
		{
			for (row = 0; row < layersize[layer]; row++) // For current layer
			{
				NeuronChrome[gene] = nLayer[layer].Bias[row]; // Save bias

				gene++;
			}
		}


		for (int layer = 0; layer < layersize.size() - 1; layer++) // For input and hidden layers
		{
			for (row = 0; row < layersize[layer]; row++) //For current layer
			{
				for (col = 0; col < layersize[layer + 1]; col++)
				{//Convert weights to chromes for current layer
					NeuronChrome[gene] = nLayer[layer].Weights[row][col];
					gene++;
				}
			}
		}

		cout << "\n";
	
	}

	return   NeuronChrome;
}

/*
	--ChromesToNeurons--
	Convert the chromes back to the neural network structure.
	Receives Island parameter which specifies which decomposition method to follow when
	passing individual to network.
*/

void NeuralNetwork::ChoromesToNeurons(Layer NeuronChrome, int Island)
{
	StringSize = NeuronChrome.size();
	int layer = 0;
	int gene = 0;

	if (Island == 1) //Neuron level
	{

		for (int neu = 0; neu < layersize[1]; neu++){

			for (int row = 0; row < layersize[layer]; row++)
			{
				nLayer[layer].Weights[row][neu] = NeuronChrome[gene];
				gene++;
			}

			nLayer[layer + 1].Bias[neu] = NeuronChrome[gene];
			gene++;
		}

		layer = 1;

		for (int neu = 0; neu < layersize[2]; neu++)
		{

			for (int row = 0; row < layersize[layer]; row++)
			{
				nLayer[layer].Weights[row][neu] = NeuronChrome[gene];
				gene++;
			}

			nLayer[layer + 1].Bias[neu] = NeuronChrome[gene];
			gene++;
		}


	}
	gene = 0;

	if (Island == 2) //Network Level (Backpropagation)
	{
		for (int layer = 1; layer < layersize.size(); layer++){
			for (row = 0; row < layersize[layer]; row++) {
				nLayer[layer].Bias[row] = NeuronChrome[gene];
				gene++;
			}


		}
		for (int layer = 0; layer < layersize.size() - 1; layer++){
			for (row = 0; row < layersize[layer]; row++){
				for (col = 0; col < layersize[layer + 1]; col++)  {
					nLayer[layer].Weights[row][col] = NeuronChrome[gene];
					gene++;
				}
			}
		}

	}
	


#ifdef hiddenNeuron
	for(int neu = 0; neu < layersize[1]; neu++ ){

		for( int row = 0; row<  layersize[layer] ; row++){
			nLayer[layer].Weights[row][neu]=  NeuronChrome[gene]  ;
			gene++;   }

		nLayer[layer+1 ].Bias[neu] =   NeuronChrome[gene]  ;
		gene++;

		for( int row = 0; row<  layersize[layer+2] ; row++){
			nLayer[layer+1].Weights[neu][row]=  NeuronChrome[gene]  ;
			gene++;}

	}


#endif


#ifdef weightlevel

	for(int layer=0; layer <  layersize.size()-1; layer++){
		for( row = 0; row<  layersize[layer] ; row++){
			for( col = 0; col <  layersize[layer+1]; col++)  {
				nLayer[layer].Weights[row][col]=  NeuronChrome[gene]  ;
				gene++; } }}
	//---------------------------------------------------------

	//-----------------------------------------------
	for(int layer=0; layer <  layersize.size() ; layer++){
		for( row = 0; row <  layersize[layer] ; row ++) {
			nLayer[layer ].Bias[row] =   NeuronChrome[gene]  ;
			gene++; }


	}
#endif




}

/*
	--ForwardFitnessPass--
	Calculate the fitness of the network with the test data
*/

double NeuralNetwork::ForwardFitnessPass(Layer NeuronChrome, TrainingExamples Test, int Island)
{

	//Convert the chromes back to neurons
	ChoromesToNeurons(NeuronChrome, Island);

	NumEval++; //number of function evaluations

	double SumErrorSquared = 0;
	bool Learn = true;


	for (int pattern = 0; pattern < Test.InputValues.size(); pattern++)
	{
		ForwardPass(Test, pattern, layersize); //Do forward pass on the input values and calculate output for the test data

	}
	

	SumErrorSquared = SumSquaredError(Test, layersize); //Calculate sum squared error from the output on the test data


	if (Learn == false)
		return -1;

	return  SumErrorSquared; //return error
}

/*
	--BP--
*/

double NeuralNetwork::BP(Layer NeuronChrome, TrainingExamples Test, int generations)
{
	double SumErrorSquared = 0;
	bool Learn = true;

	for (int epoch = 0; epoch < generations; epoch++){

		for (int pattern = 0; pattern < Test.InputValues.size(); pattern++)
		{
			ForwardPass(Test, pattern, layersize);

		}
		for (int pattern = 0; pattern < Test.InputValues.size(); pattern++)
		{
			BackwardPass(Test, .1, pattern, layersize);
			//send neurons to chrome.......
		}
	}
	SumErrorSquared = SumSquaredError(Test, layersize);

	return  SumErrorSquared;
}

/***************************************************************************************

			--4. Individual--		
		
	Represents a particular chromozone

***************************************************************************************/
class Individual
{
	//Class variables
public:

	Layer Chrome;
	double Fitness;
	Layer BitChrome;

public:
	Individual()
	{

	}
	void print();


};

typedef vector<double> Nodes;


/***************************************************************************************

			--5. GeneticAlgorithmn--		
		
Used to evolve a population in order to achieve a set which has an acceptable fitness level

***************************************************************************************/

class GeneticAlgorithmn :public virtual RandomNumber 
{
	//Class variables
	public:
		int PopSize;
		vector<Individual> Population;
		Sizes TempIndex;
		vector<Individual> NewPop;
		Sizes mom;
		Sizes list;
		int MaxGen;
		int NumVariable;
		double BestFit;
		int BestIndex;
		int NumEval;
		int  kids;

	public:

		GeneticAlgorithmn(int stringSize)
		{
			NumVariable = stringSize;
			NumEval = 0;
			BestIndex = 0;

		}
		GeneticAlgorithmn()
		{
			BestIndex = 0;
		}

		double Fitness() { return BestFit; }
		double  RandomWeights();
		double  RandomAddition();
		void PrintPopulation();
		int GenerateNewPCX(int pass, NeuralNetwork network, TrainingExamples Sample, double Mutation, int depth);
		double Objective(Layer x);
		void  InitilisePopulation(int popsize);
		void Evaluate();
		double  modu(double index[]);

		//Calculates the inner product of two vectors
		double  innerprod(double Ind1[], double Ind2[]);
		double RandomParents();
		double MainAlgorithm(double RUN, ofstream &out1, ofstream &out2, ofstream &
			out3);
		double Noise();
		void  my_family();   //Here a random family (1 or 2) of parents is created who would be replaced by good individuals
		void  find_parents();
		void  rep_parents();  //Here the best (1 or 2) individuals replace the family of parents
		void sort();

};

/*
	--RandomWeights--
	Generate random number  between -5 and 5(double)
*/

double GeneticAlgorithmn::RandomWeights()
{
	int chance;
	double randomWeight;
	double NegativeWeight;
	chance = rand() % 2;

	if (chance == 0){
		randomWeight = rand() % 100000;
		return randomWeight*0.00005;
	}

	if (chance == 1){
		NegativeWeight = rand() % 100000;
		return NegativeWeight*-0.00005;
	}

}

/*
	--RandomAddition--
	Generate random number  between -0.9 and 0.9 (type double)
*/


double GeneticAlgorithmn::RandomAddition()
{
	int chance;
	double randomWeight;
	double NegativeWeight;
	chance = rand() % 2;

	if (chance == 0){
		randomWeight = rand() % 100;
		return randomWeight*0.009;
	}

	if (chance == 1){
		NegativeWeight = rand() % 100;
		return NegativeWeight*-0.009;
	}

}

/*
	--InitializsePopulation--
	Intialize the vectors used in algorithm,
*/

void GeneticAlgorithmn::InitilisePopulation(int popsize)
{

	//variable declaration
	double x, y;
	Individual Indi ;
	NumEval=0;
    BestIndex = 0;
    PopSize = popsize;
	
	for (int i = 0; i < PopSize; i++){ 
        TempIndex.push_back(0); //initialize with 0s
        mom.push_back(0); //initialize female parent 
	}

	for (int i = 0; i < NPSize; i++){  	
		list.push_back(0); //initialize with 0s
	}

	for(int row = 0; row < PopSize  ; row++) 
        Population.push_back(Indi); //population made up of individuals

	for(int row = 0; row < PopSize  ; row++) {
		for(int col = 0; col < NumVariable ; col++){
  
			Population[row].Chrome.push_back(RandomWeights());//initialize population with random weights
		}
	}

	for(int row = 0; row < NPSize  ; row++) 
		NewPop.push_back(Indi);//new population made up of individuals

	for(int row = 0; row < NPSize  ; row++) {
		for(int col = 0; col < NumVariable ; col++)
			NewPop[row].Chrome.push_back(0);//initialise new population with 0s

	}
     

}

/*
	--Evaluate--
	Calculates the fitness of all chromozones
*/ 

void GeneticAlgorithmn::Evaluate()
{
	//Solutions are evaluated and best id is computed

	Population[0].Fitness = Objective(Population[0].Chrome); //calculate fitness by using objective function
	BestFit = Population[0].Fitness; //Initially best fitness is the fitness of the first chromozone
	BestIndex = 0;

	for (int row = 0; row < PopSize; row++)
	{
		Population[row].Fitness = Objective(Population[row].Chrome); //Evaluate fitness of chromes
		if ((MINIMIZE * BestFit) >(MINIMIZE * Population[row].Fitness))
		{
			BestFit = Population[row].Fitness; //Update best fitness
			BestIndex = row;
		}
	}

}

/*
	--Print--
	Output fitness and chromozone values
*/ 
void GeneticAlgorithmn::PrintPopulation()
{
	for(int row = 0; row < PopSize/5   ; row++) {
		
		for(int col = 0; col < NumVariable ; col++)
			cout<< Population[row].Chrome[col]<<" "; //output individuals and its chromes
		
		cout<<endl;
	}

	for(int row = 0; row < PopSize/5  ; row++)
		cout<< Population[row].Fitness<<endl;//output fitness

	cout<<" ---"<<endl;
	cout<<BestFit<<"  "<<BestIndex<<endl;//best fitness
}

/*
	--Objective--
	Used to calculate the fitness of the chromozones. Contains 3 functions which can be used based on their need
*/

double GeneticAlgorithmn::Objective(Layer x)
{

	return 0;
}

/*
	--my_family--
	Here a random family (1 or 2) of parents is created who would be replaced by good individuals
*/

void GeneticAlgorithmn::my_family()   //here a random family (1 or 2) of parents is created who would be replaced by good individuals
{
	int i,j,index; //variables
	int swp;
	double u;

	for(i=0;i<PopSize;i++)
		mom[i]=i;

	for(i=0;i<family;i++)
	    {
			index = (rand()%PopSize) +i; //randomly get index value which corresponds to an individual from the population

			if(index>(PopSize-1)) index=PopSize-1; //make sure index is not outside population range
			swp=mom[index];
			mom[index]=mom[i];
			mom[i]=swp; //swap values
	    }
}

/*
	--find_parents--
	Here the parents to be replaced are added to the temporary sub-population to assess their goodness  against the new solutions formed which will be the basis of whether they should be kept or not
*/


void GeneticAlgorithmn::find_parents()  
{
	int i, j, k;
	double u, v;

	my_family();
	//cout<<kids<<endl;
	for (j = 0; j < family; j++)
	{
		for (i = 0; i < NumVariable; i++)
			NewPop[kids + j].Chrome[i] = Population[mom[j]].Chrome[i];

		NewPop[kids + j].Fitness = Objective(NewPop[kids + j].Chrome);

	}
}

/*
	--rep_parents--
	Here the best (1 or 2) individuals replace the family of parents
*/

void GeneticAlgorithmn::rep_parents()   
{
	int i, j;
	for (j = 0; j < family; j++)
	{
		for (i = 0; i < NumVariable; i++)
			Population[mom[j]].Chrome[i] = NewPop[list[j]].Chrome[i];  //New child added to population

		Population[mom[j]].Fitness = Objective(Population[mom[j]].Chrome); //Update fitness

	}
}

/*
	--sort--
	Arrange individuals in order of population
*/

void GeneticAlgorithmn::sort()

{
	int i, j, temp;
	double dbest;

	for (i = 0; i < (kids + family); i++) list[i] = i;

	if (MINIMIZE)
	for (i = 0; i < (kids + family - 1); i++)
	{
		dbest = NewPop[list[i]].Fitness;
		for (j = i + 1; j < (kids + family); j++)
		{
			if (NewPop[list[j]].Fitness < dbest)
			{
				dbest = NewPop[list[j]].Fitness; //get the best fitness
				temp = list[j];
				list[j] = list[i];
				list[i] = temp;
			}
		}
	}
	else
	for (i = 0; i < (kids + family - 1); i++)
	{
		dbest = NewPop[list[i]].Fitness;
		for (j = i + 1; j<(kids + family); j++)
		{
			if (NewPop[list[j]].Fitness > dbest)
			{
				dbest = NewPop[list[j]].Fitness; //get the best fitness
				temp = list[j];
				list[j] = list[i];
				list[i] = temp;
			}
		}
	}
}

/*
	--modu--
	Calculate modulus
*/

double GeneticAlgorithmn::modu(double index[])
{
	int i;
	double sum, modul;

	sum = 0.0;
	for (i = 0; i < NumVariable; i++)
		sum += (index[i] * index[i]);

	modul = sqrt(sum);
	return modul;
}

/*
	--innerprod--
	calculates the inner product of two vectors
*/

double GeneticAlgorithmn::innerprod(double Ind1[], double Ind2[])
{
	int i;
	double sum;

	sum = 0.0;

	for (i = 0; i < NumVariable; i++)
		sum += (Ind1[i] * Ind2[i]);

	return sum;
}

/*
	--GenerateNewPCX--
	Generate new population
*/

int GeneticAlgorithmn::GenerateNewPCX(int pass, NeuralNetwork network, TrainingExamples Sample, double Mutation, int depth)
{
	int i, j, num, k;  double Centroid[NumVariable];
	double tempvar, tempsum, D_not, dist;
	double tempar1[NumVariable];
	double tempar2[NumVariable];
	double D[RandParent];
	double d[NumVariable];
	double diff[RandParent][NumVariable];
	double temp1, temp2, temp3;
	int temp;

	for (i = 0; i < NumVariable; i++)
		Centroid[i] = 0.0; //initialize centroid with 

	// centroid is calculated here
	for (i = 0; i < NumVariable; i++)
	{
		for (j = 0; j < RandParent; j++)
			Centroid[i] += Population[TempIndex[j]].Chrome[i];

		Centroid[i] /= RandParent;

	}

	// calculate the distace (d) from centroid to the index parent arr1[0]
	// also distance (diff) between index and other parents are computed
	for (j = 1; j < RandParent; j++)
	{
		for (i = 0; i < NumVariable; i++)
		{
			if (j == 1)
				d[i] = Centroid[i] - Population[TempIndex[0]].Chrome[i];
			diff[j][i] = Population[TempIndex[j]].Chrome[i] - Population[TempIndex[0]].Chrome[i];
		}
		if (modu(diff[j]) < EPSILON)
		{
			cout << "RUN Points are very close to each other. Quitting this run   " << endl;

			return (0);
		}

		if (isnan(diff[j][i]))
		{
			cout << "`diff nan   " << endl;
			diff[j][i] = 1;
			return (0);
		}


	}
	dist = modu(d); // modu calculates the magnitude of the vector

	if (dist < EPSILON)
	{
		cout << "RUN Points are very close to each other. Quitting this run    " << endl;

		return (0);
	}

	// orthogonal directions are computed (see the paper)
	for (i = 1; i < RandParent; i++)
	{
		temp1 = innerprod(diff[i], d);
		if ((modu(diff[i])*dist) == 0){
			cout << " division by zero: part 1" << endl;
			temp2 = temp1 / (1);
		}
		else{
			temp2 = temp1 / (modu(diff[i])*dist);
		}

		temp3 = 1.0 - pow(temp2, 2.0);
		D[i] = modu(diff[i])*sqrt(temp3);
	}

	D_not = 0;
	for (i = 1; i < RandParent; i++)
		D_not += D[i];

	D_not /= (RandParent - 1); //this is the average of the perpendicular distances from all other parents (minus the index parent) to the index vector

	// Next few steps compute the child, by starting with a random vector
	for (j = 0; j < NumVariable; j++)
	{
		tempar1[j] = noise(0.0, (D_not*sigma_eta));
		//tempar1[j] = Noise();
		tempar2[j] = tempar1[j];
	}

	for (j = 0; j < NumVariable; j++)
	{
		if (pow(dist, 2.0) == 0){
			cout << " division by zero: part 2" << endl;
			tempar2[j] = tempar1[j] - ((innerprod(tempar1, d)*d[j]) / 1);
		}
		else
			tempar2[j] = tempar1[j] - ((innerprod(tempar1, d)*d[j]) / pow(dist, 2.0));
	}

	for (j = 0; j < NumVariable; j++)
		tempar1[j] = tempar2[j];

	for (k = 0; k < NumVariable; k++)
		NewPop[pass].Chrome[k] = Population[TempIndex[0]].Chrome[k] + tempar1[k];

	tempvar = noise(0.0, (sigma_zeta));


	for (k = 0; k < NumVariable; k++){
		NewPop[pass].Chrome[k] += (tempvar*d[k]);

	}

	double random = rand() % 10;

	Layer Chrome(NumVariable);

	for (k = 0; k < NumVariable; k++){
		if (!isnan(NewPop[pass].Chrome[k])){
			Chrome[k] = NewPop[pass].Chrome[k];
		}
		else
			NewPop[pass].Chrome[k] = RandomAddition();

	}

	return (1);
}

/*
	--RandomParents--
	Select two parents. First one will be the one with the best fitness within the population and the second one is randomly selected
*/

double GeneticAlgorithmn::RandomParents()
{

	int i, j, index;
	int swp;
	double u;
	int delta;

	for (i = 0; i < PopSize; i++)
		TempIndex[i] = i;

	swp = TempIndex[0];
	TempIndex[0] = TempIndex[BestIndex];  // best is always included as a parent and is the index parent
	// this can be changed for solving a generic problem
	TempIndex[BestIndex] = swp;

	for (i = 1; i<RandParent; i++)  // shuffle the other parents
	{
		//u=randomperc();
		index = (rand() % PopSize) + i;

		if (index>(PopSize - 1)) index = PopSize - 1;
		swp = TempIndex[index];
		TempIndex[index] = TempIndex[i];
		TempIndex[i] = swp;
	}


}


void TIC(void)
{
	TicTime = time(NULL);
}

void TOC(void)
{
	TocTime = time(NULL);
}

double StopwatchTimeInSeconds()
{
	return difftime(TocTime, TicTime);
}

/*
	--MainAlgorithm--
	test genetic algorithm here
*/


double GeneticAlgorithmn::MainAlgorithm(double RUN, ofstream &out1, ofstream &out2, ofstream &out3)
{
	TIC();
	double tempfit = 0;
	int count = 0;
	int tag;
	kids = KIDS;

	int gen = MAXFUN / kids;
	gen = 300000;
	basic_seed = 0.4122;   //arbitrary choice

	seed = basic_seed + (1.0 - basic_seed)*(double)(RUN - 1) / (double)MAXRUN;
	if (seed > 1.0) printf("\n warning!!! seed number exceeds 1.0");
	randomize(seed);

	InitilisePopulation(100);
	Evaluate();

	tempfit = Population[BestIndex].Fitness;

	for (count = 1; ((count <= gen) && (tempfit >= LIMIT)); count++)
	{
		//   cout<<count<<endl;



		RandomParents();           //random array of parents to do PCX is formed

		// for(int i = 0; i < PopSize; i++)
		//  cout<<TempIndex[i]<< " ";
		//  cout<<endl;

		for (int i = 0; i < kids; i++)
		{
			//  tag = GenerateNewPCX(i,network, Samples); //generate a child using PCX
			if (tag == 0) break;
		}
		if (tag == 0) break;
		// PrintPopulation();

		find_parents();  // form a pool from which a solution is to be
		//   replaced by the created child

		sort();          // sort the kids+parents by fitness

		rep_parents();   // a chosen parent is replaced by the child

		//finding the best in the population
		BestIndex = 0;
		tempfit = Population[0].Fitness;
		cout << tempfit << " is temp fit" << endl;
		for (int i = 1; i < PopSize; i++)
		if ((MINIMIZE * Population[i].Fitness) < (MINIMIZE * tempfit))
		{
			tempfit = Population[i].Fitness;
			BestIndex = i;

			//	cout<<"yees  "<<BestIndex<<endl;
		}

		// print out results after every 100 generations
		if (((count % 100) == 0) || (tempfit <= LIMIT))
			out1 << count*kids << "    " << tempfit << endl;

		cout << count << " --------" << tempfit << endl;

		//for(int i=0;i<	NumVariable;i++)
		// cout<<Population[BestIndex].Chrome[i]<<" ";
		//cout<<endl;
	}

	TOC();
	//  cout<<" Run Number: "<<RUN<<endl;
	// out2<<"Best solution obtained after X function evaluations:"<<count*kids<<" "<<NumEval<<endl;
	out2 << RUN << "   ";
	for (int i = 0; i < NumVariable; i++)
		out2 << Population[BestIndex].Chrome[i] << "      ";
	out2 << "---->  " << tempfit << "     " << count*kids << "      " << StopwatchTimeInSeconds() << endl;

	//out2<<"Fitness of this best solution:"<<tempfit<<endl;

	cout << "Best solution obtained after X function evaluations:" << count*kids << " " << NumEval << endl;

	for (int i = 0; i < NumVariable; i++)
		cout << Population[BestIndex].Chrome[i] << " ";
	cout << endl;

	cout << "Fitness of this best solution:" << tempfit << " " << StopwatchTimeInSeconds() << endl;

}
//--------------------------------------------------------------------------------------------


typedef vector<GeneticAlgorithmn> GAvector;

/***************************************************************************************

			--6. Table--		

***************************************************************************************/

class Table
{
	public:
	Layer   SingleSp;
};

typedef vector<Table> TableVector;


/***************************************************************************************

			--7. CoEvolution--		

***************************************************************************************/

class CoEvolution : public  NeuralNetwork, public virtual TrainingExamples, public   GeneticAlgorithmn, public virtual RandomNumber
{

public:

	int 	NoSpecies;
	GAvector Species;

	TableVector  TableSp;
	int PopSize;
	vector<bool> NotConverged;
	Sizes SpeciesSize;
	Layer   Individual;
	Layer  BestIndividual;
	double bestglobalfit;
	Data TempTable;
	int TotalEval;
	int TotalSize;
	int SingleGAsize;
	double Train;
	double Test;
	int kid;

	CoEvolution(){

	}

	void	MainProcedure(bool bp, int RUN, ofstream &out1, ofstream &out2, ofstream &out3, double mutation, int depth);
	void  	InitializeSpecies(int popsize);
	void  	EvaluateSpecies(NeuralNetwork network, TrainingExamples Sample, int Island);
	void    GetBestTable(int sp);
	void 	PrintSpecies();
	void   	Join();
	double 	ObjectiveFunc(Layer x);
	void 	Print();
	void   	sort(int s);
	void  	find_parents(int s, NeuralNetwork network, TrainingExamples Sample, int Island);
	void 	EvalNewPop(int pass, int s, NeuralNetwork network, TrainingExamples Sample, int Island);
	void  	rep_parents(int s, NeuralNetwork network, TrainingExamples Sample, int Island);
	void   	EvolveSubPopulations(int repetitions, double h, NeuralNetwork network, TrainingExamples Sample, double mutation, int depth, ofstream &out2, int Island);

};

/*
	--InitializeSpecies--
	Initialize the sub-populations in the CC framework
*/

void    CoEvolution::InitializeSpecies(int popsize)
{
	 //Variable declaration 
	PopSize = popsize;

	GAvector SpeciesP(NoSpecies); //Species of type Genetic Algorithm 
	Species = SpeciesP;

	for (int Sp = 0; Sp < NoSpecies; Sp++){
		NotConverged.push_back(false);
	}

	basic_seed = 0.4122;
	RUN = 2;

	seed = basic_seed + (1.0 - basic_seed)*(double)((RUN)-1) / (double)MAXRUN;
	if (seed>1.0) printf("\n warning!!! seed number exceeds 1.0");

	for (int s = 0; s < NoSpecies; s++){

		Species[s].randomize(seed);

	}

	TotalSize = 0;
	for (int row = 0; row < NoSpecies; row++)
		TotalSize += SpeciesSize[row];

	for (int row = 0; row < TotalSize; row++)
		Individual.push_back(0); //initialize with 0s

	for (int s = 0; s < NoSpecies; s++){

		Species[s].NumVariable = SpeciesSize[s];
		Species[s].InitilisePopulation(popsize); //initialize species with 0s...that is the individuals to 0s
	}

	TableSp.resize(NoSpecies); //resize to number of sub-pops--- to store the combined species from each of the sub-pops

	for (int row = 0; row < NoSpecies; row++)
	for (int col = 0; col < SpeciesSize[row]; col++)
		TableSp[row].SingleSp.push_back(0); //resize to the size of the species in each sub-pop


}

/*
	--PrintSpecies--
	Print the individuals within the population
*/

void    CoEvolution::PrintSpecies()
{

	for (int s = 0; s < NoSpecies; s++){
		Species[s].PrintPopulation();
		cout << s << endl;
	}

}

/*
	--GetBestTable--
	Populate TableSp with best species
	
*/

void    CoEvolution::GetBestTable(int CurrentSp)
{
	int Best;

	for (int sN = 0; sN < CurrentSp; sN++)
	{
		Best = Species[sN].BestIndex; //Best index
		for (int s = 0; s < SpeciesSize[sN]; s++)
			//Get best set of chromes
			TableSp[sN].SingleSp[s] = Species[sN].Population[Best].Chrome[s];
	}

	for (int sN = CurrentSp; sN < NoSpecies; sN++)
	{
		Best = Species[sN].BestIndex;
		for (int s = 0; s < SpeciesSize[sN]; s++)
			TableSp[sN].SingleSp[s] = Species[sN].Population[Best].Chrome[s];
	}
}

/*
	--Join--
	Join all the sub-populations together
*/

void   CoEvolution::Join()
{
	int index = 0;

	for (int row = 0; row < NoSpecies; row++){
		for (int col = 0; col < SpeciesSize[row]; col++){
			Individual[index] = TableSp[row].SingleSp[col]; //Join the species to form one individual	
			index++;
		}
	}
}

/*
	--Print--
*/

void   CoEvolution::Print()
{
	for (int row = 0; row < NoSpecies; row++){
		for (int col = 0; col < SpeciesSize[row]; col++){
			cout << TableSp[row].SingleSp[col] << " ";
		}
		cout << endl;
	}
	cout << endl;

	for (int row = 0; row < TotalSize; row++)
		cout << Individual[row] << " ";
	cout << endl << endl;
}

/*
	--EvaluateSpecies--
*/

void    CoEvolution::EvaluateSpecies(NeuralNetwork network, TrainingExamples Sample, int Island)
{


	for (int SpNum = 0; SpNum < NoSpecies; SpNum++){

		GetBestTable(SpNum);

		//---------make the first individual in the population the best

		for (int i = 0; i < Species[SpNum].NumVariable; i++)
			TableSp[SpNum].SingleSp[i] = Species[SpNum].Population[0].Chrome[i];

		Join();
		Species[SpNum].Population[0].Fitness = network.ForwardFitnessPass(Individual, Sample, Island);//ObjectiveFunc(Individual);
		TotalEval++;

		Species[SpNum].BestFit = Species[SpNum].Population[0].Fitness;
		Species[SpNum].BestIndex = 0;
		// cout<<"g"<<endl;
		//------------do for the rest

		for (int PIndex = 0; PIndex < PopSize; PIndex++){


			for (int i = 0; i < Species[SpNum].NumVariable; i++)
				TableSp[SpNum].SingleSp[i] = Species[SpNum].Population[PIndex].Chrome[i];


			Join();
			//Print();

			Species[SpNum].Population[PIndex].Fitness = network.ForwardFitnessPass(Individual, Sample, Island);//
			TotalEval++;
			//   ObjectiveFunc(Individual);

			if ((MINIMIZE * Species[SpNum].BestFit) > (MINIMIZE * Species[SpNum].Population[PIndex].Fitness))
			{
				Species[SpNum].BestFit = Species[SpNum].Population[PIndex].Fitness;
				Species[SpNum].BestIndex = PIndex;
				//  cout<<Species[SpNum].Population[PIndex].Fitness<<endl;
			}

		}

		// cout<< Species[SpNum].BestIndex<<endl;
		cout << SpNum << " -- " << endl;
	}
}

double   CoEvolution::ObjectiveFunc(Layer x)
{
	int i, j, k;
	double fit, sumSCH;

	return(fit);
}

/*
	--find_parents--
	Here the parents to be replaced are added to the temporary sub-population to assess 
	their goodness against the new solutions formed which will be the basis of whether they should be kept or not
*/

void CoEvolution::find_parents(int s, NeuralNetwork network, TrainingExamples Sample, int Island)   
{
	int i, j, k;
	double u, v;

	Species[s].my_family();

	for (j = 0; j < family; j++)
	{

		Species[s].NewPop[Species[s].kids + j].Chrome = Species[s].Population[Species[s].mom[j]].Chrome; //Add parents to new population



		GetBestTable(s); //Get best species
		for (int i = 0; i < Species[s].NumVariable; i++)
			TableSp[s].SingleSp[i] = Species[s].NewPop[Species[s].kids + j].Chrome[i]; //Get species into table used by join() to concatenate 
		Join();	//Concatenate to form one individual

		Species[s].NewPop[Species[s].kids + j].Fitness = network.ForwardFitnessPass(Individual, Sample, Island); //Evalaute fitness of new addition which are the parents
		TotalEval++;
	}
}

/*
	--EvalNewPop--
	Evaluate fitness of new sub-populations
*/

void CoEvolution::EvalNewPop(int pass, int s, NeuralNetwork network, TrainingExamples Sample, int Island)
{
	GetBestTable(s);
	for (int i = 0; i < Species[s].NumVariable; i++)
		TableSp[s].SingleSp[i] = Species[s].NewPop[pass].Chrome[i];
	Join();

	Species[s].NewPop[pass].Fitness = network.ForwardFitnessPass(Individual, Sample, Island);
	TotalEval++;

}

/*
	--sort--
	Sorts the species in  the sub-population in order of fitness
*/

void  CoEvolution::sort(int s)

{
	int i, j, temp;
	double dbest;

	for (i = 0; i < (Species[s].kids + family); i++) Species[s].list[i] = i;

	if (MINIMIZE)
	for (i = 0; i < (Species[s].kids + family - 1); i++)
	{
		dbest = Species[s].NewPop[Species[s].list[i]].Fitness;
		for (j = i + 1; j < (Species[s].kids + family); j++)
		{
			if (Species[s].NewPop[Species[s].list[j]].Fitness < dbest)
			{
				dbest = Species[s].NewPop[Species[s].list[j]].Fitness;
				temp = Species[s].list[j];
				Species[s].list[j] = Species[s].list[i];
				Species[s].list[i] = temp;
			}
		}
	}
	else
	for (i = 0; i < (Species[s].kids + family - 1); i++)
	{
		dbest = Species[s].NewPop[Species[s].list[i]].Fitness;
		for (j = i + 1; j<(Species[s].kids + family); j++)
		{
			if (Species[s].NewPop[Species[s].list[j]].Fitness > dbest)
			{
				dbest = Species[s].NewPop[Species[s].list[j]].Fitness;
				temp = Species[s].list[j];
				Species[s].list[j] = Species[s].list[i];
				Species[s].list[i] = temp;
			}
		}
	}
}

/*
	--rep_parents--
	Here the best (1 or 2) individuals replace the family of parents
*/

void CoEvolution::rep_parents(int s, NeuralNetwork network, TrainingExamples Sample, int Island)   
{
	int i, j;
	for (j = 0; j < family; j++)
	{

		Species[s].Population[Species[s].mom[j]].Chrome = Species[s].NewPop[Species[s].list[j]].Chrome; //Update population with new species

		GetBestTable(s); //Update fitness and update best

		for (int i = 0; i < Species[s].NumVariable; i++)
			TableSp[s].SingleSp[i] = Species[s].Population[Species[s].mom[j]].Chrome[i];	//Get into one table for concatenation
		Join();	//Concatenate into one individual

		Species[s].Population[Species[s].mom[j]].Fitness = network.ForwardFitnessPass(Individual, Sample, Island); //Update fitness of new individual
		TotalEval++;
	}
}

/*
	--EvolveSubPopulations--
	Evolve and form new sub-populations
*/

void CoEvolution::EvolveSubPopulations(int repetitions, double h, NeuralNetwork network, TrainingExamples Samples, double mutation, int depth, ofstream & out1, int Island)
{
	double tempfit;
	int count = 0;
	int tag = 0;
	kid = KIDS;
	int numspecies = 0;

	for (int s = 0; s < NoSpecies; s++) 
	{
		if (NotConverged[s] == true)
		{
			for (int r = 0; r < repetitions; r++)
			{

				tempfit = Species[s].Population[Species[s].BestIndex].Fitness;
				Species[s].kids = KIDS;
				Species[s].RandomParents();

				for (int i = 0; i < Species[s].kids; i++)
				{
					tag = Species[s].GenerateNewPCX(i, network, Samples, mutation, depth); //generate a child using PCX

					if (tag == 0) 
					{
						NotConverged[s] = false;
				
						break;
					}
				}


				if (tag == 0) 
				{
			
					NotConverged[s] = false;
				}


				for (int i = 0; i < Species[s].kids; i++)
					EvalNewPop(i, s, network, Samples, Island);


				find_parents(s, network, Samples, Island);  // form a pool from which a solution is to be
				//   replaced by the created child

				Species[s].sort();          // sort the kids+parents by fitness
		
				rep_parents(s, network, Samples, Island);   // a chosen parent is replaced by the child


				Species[s].BestIndex = 0;

				tempfit = Species[s].Population[0].Fitness;

				for (int i = 1; i < PopSize; i++)
				if ((MINIMIZE *    Species[s].Population[i].Fitness) < (MINIMIZE * tempfit))
				{
					tempfit = Species[s].Population[i].Fitness;
					Species[s].BestIndex = i;
				}
				
			}

		}//r

	}//species
}

/***************************************************************************************

			--8. CombinedEvolution--		

***************************************************************************************/
class CombinedEvolution : public    CoEvolution
{

public:
	int TotalEval;
	int TotalSize;
	double Energy;
	int SAFuncEval;
	double Train;
	double Test;
	double Error;
	CoEvolution NeuronLevel;
	CoEvolution ReverseNeuronLevel;
	CoEvolution WeightLevel;
	CoEvolution OneLevel;
	CoEvolution LayerLevel;
	int Cycles;
	bool Sucess;

	CombinedEvolution(){

	}

	int GetEval(){
		return TotalEval;
	}
	int GetCycle(){
		return Cycles;
	}
	double GetError(){
		return Error;
	}

	bool GetSucess(){
		return Sucess;
	}

	void   Procedure(bool bp, double h, ofstream &out1, ofstream &out2, ofstream &out3, double mutation, int depth);

};


/*
	--Procedure--
	The entire algorithm
*/

void    CombinedEvolution::Procedure(bool bp, double h, ofstream &out1, ofstream &out2, ofstream &out3, double mutation, int depth)
{
	bool usememe = true;

	if (depth == 0)
		usememe = false;

	clock_t start = clock();

	int hidden = h;

	int weightsize1 = (input*hidden); 	//Number of weights between hidden and input layer
	int weightsize2 = (hidden*output);	//Number of weights between hidden and output layer
	int contextsize = hidden*hidden;
	int biasize = hidden + output;		//Bias for hidden and output layer

	ofstream out;
	out.open("Rnuuu.txt");

	double trainpercent = 0;
	double testpercent = 0;
	int epoch;
	double testtree;
	char  file[15] = "Learnt.txt"; //File to save learned data via SaveLearnedData()

	double H = 0;
	int gene = 1;

	TrainingExamples Samples(trainfile, trainsize, input + output, input, output);  //Get training data
	Samples.printData(); //Print the data
	double error;

	Sizes layersize;
	layersize.push_back(input); //Set size(neurons) of input layer
	layersize.push_back(hidden); //Set size(neurons) of hidden layer--only one hidden layer
	layersize.push_back(output); //Set size(neurons) of output layer


	NeuralNetwork network(layersize); //Create network with appropriate dimensions
	network.CreateNetwork(layersize, Samples); //Populate network with samples

	TotalEval = 0;

	if (bp) //If performing standalone Backpropagation
	{
		gene = network.BackPropogation(Samples, 0.1, layersize, file, true);
		Train = network.TestTrainingData(layersize, trainfile, trainsize, file, input, output);
		Test = network.TestLearnedData(layersize, testfile, testsize, file, input, output); //Test the network
	
	}
	else
	{
		Sucess = false;

		Cycles = 0;

		// >>>>>>>>>>>>>>>>> Initialize Neuron Level Island <<<<<<<<<<<< //

		for (int n = 0; n < hidden; n++)
			NeuronLevel.SpeciesSize.push_back(input + 1); //Set size of species between input and hidden layer (total input neurons + 1 bias)

		for (int n = 0; n < output; n++)
			NeuronLevel.SpeciesSize.push_back(hidden + 1); //Set size of species between hidden and output layer (total hidden neurons + 1 bias)

		NeuronLevel.NoSpecies = hidden + output; //Set size of all species in neuronlevel(total hidden neurons + total output neurons).
		NeuronLevel.InitializeSpecies(CCPOPSIZE); //Generate random populations for each species.
		NeuronLevel.EvaluateSpecies(network, Samples, 1); //Get fitness of populations in species

		cout << "Hidden: " << h << " Total Sub-pops: " << NeuronLevel.NoSpecies << endl;

		for (int s = 0; s < NeuronLevel.NoSpecies; s++)
			NeuronLevel.NotConverged[s] = true;

		cout << " Evaluated Neuronlevel ----------->" << endl;
	
		// >>>>>>>>>>>>>>>>> Initialize Backpropagation Level Island <<<<<<<<<<<< //

		OneLevel.SpeciesSize.push_back(weightsize1 + weightsize2 + biasize);
		OneLevel.NoSpecies = 1;
		OneLevel.InitializeSpecies(1);

		int m = 0;

		// Equalize both Islands by Passing individual from first population in all species for neuron level to backpropagation island
		for (int sp = 0; sp < NeuronLevel.NoSpecies; sp++)
		{
			for (int col = 0; col < NeuronLevel.SpeciesSize[sp]; col++)
			{
				OneLevel.Species[0].Population[0].Chrome[m] = NeuronLevel.Species[sp].Population[0].Chrome[col];
				m++;
			}
		}
		

		OneLevel.EvaluateSpecies(network, Samples, 2);
		
		for (int s = 0; s < OneLevel.NoSpecies; s++)
			OneLevel.NotConverged[s] = true;

		cout << " Evaluated OneLevel ----------->" << endl;


		// >>>>>>>>>>>>>>>>> 2- Island Competition. Start Island Eovlution <<<<<<<<<<<< //

		//Set Total eval of each island to 0
		TotalEval = 0;
		NeuronLevel.TotalEval = 0;
		OneLevel.TotalEval = 0;

		int NeuronTempEval = 0;
		int count = 10;

		double  OneLevelError = -1;
		double OneLevelTrain = -1;

		Layer BestMemes(10000);
		Layer ErrorArray;

		double BestWL, BestLL, BestNetL;

		int total_epoch = 0;

		//Generate Backpropagation individual from onelevel chromes
		OneLevel.GetBestTable(OneLevel.NoSpecies - 1);
		OneLevel.Join();

		int winner_island = 2;

		while (TotalEval <= maxgen) //While sum of evals in both islands is less then max generation (15000)
		{
			double NeuronLevelError = 10000;
			double WeightLevelError = 10000;
			double NetLevelError = 10000;
			double NeuronLevelTrain = 0;
			double  WeightLevelTrain = 0;

			int neutral = 10;

			// >>>>>>>>>>>>>>>>>>>>>>>>>>> COMPETITION <<<<<<<<<<<<<<<<<<<<<<< //

			// >>>>>>>>>>>>>>>>> Island 1 Evolution: Backpropagation <<<<<<<<<<<< //

			//Pass in onelevel individual to be refined by backpropagation
			OneLevel.Individual = Layer(network.BackpropogationCICC(OneLevel.Individual, Samples, 0.1, layersize, file, true, 20, 2));
			total_epoch += 20;
			network.ChoromesToNeurons(OneLevel.Individual, 2);

			NetLevelError = network.SumSquaredError(Samples, layersize); //Get network error

			BestNetL = NetLevelError;


			cout << "BP Island Evals  " << total_epoch << " Error: " << BestNetL << endl;

			// >>>>>>>>>>>>>>>>> Island 2 Evolution: Neural Level <<<<<<<<<<<< //

			NeuronLevel.EvolveSubPopulations(1, 1, network, Samples, mutation, 0, out2, 1);
			NeuronLevel.GetBestTable(NeuronLevel.NoSpecies - 1);
			NeuronLevel.Join();

			network.ChoromesToNeurons(NeuronLevel.Individual, 1);
			network.SaveLearnedData(layersize, file);

			NeuronLevelError = NeuronLevel.Species[NeuronLevel.NoSpecies - 1].Population[NeuronLevel.Species[NeuronLevel.NoSpecies - 1].BestIndex].Fitness;

			cout << "Neuron Island Evals: " << NeuronLevel.TotalEval << " Error: " << NeuronLevelError << endl;

			BestLL = NeuronLevelError;


			count++;

			// >>>>>>>>>>>>>>>>>>>>>>>>>>> COLLABORATION <<<<<<<<<<<<<<<<<<<<<<< //

			if (BestNetL < BestLL) //If BP Wins. Copy to NL
			{ 
				cout << "\n<------- NetL wins ------>\n";
			
				network.ChoromesToNeurons(OneLevel.Individual, 2); //Copy BP individual to network
				NeuronLevel.Individual = network.Neurons_to_chromes(1); //Set neuron level individual from current network individual

				// Pass neuronlevel individual back into species 

				int index = 0;

				for (int row = 0; row < NeuronLevel.NoSpecies; row++)
				{
					for (int col = 0; col < NeuronLevel.SpeciesSize[row]; col++)
					{
						NeuronLevel.TableSp[row].SingleSp[col] = NeuronLevel.Individual[index];
						index++;
					}

				}

				int Best = NeuronLevel.Species[NeuronLevel.NoSpecies - 1].BestIndex;

				for (int sN = 0; sN < NeuronLevel.NoSpecies - 1; sN++)
				{
		
					for (int s = 0; s < NeuronLevel.SpeciesSize[sN]; s++)
						NeuronLevel.Species[sN].Population[Best].Chrome[s] = NeuronLevel.TableSp[sN].SingleSp[s];
					NeuronLevel.Species[sN].BestIndex = Best;
				}

				for (int sN = NeuronLevel.NoSpecies - 1; sN < NeuronLevel.NoSpecies; sN++)
				{
					for (int s = 0; s < NeuronLevel.SpeciesSize[sN]; s++)
						NeuronLevel.Species[sN].Population[Best].Chrome[s] = NeuronLevel.TableSp[sN].SingleSp[s];
					NeuronLevel.Species[sN].BestIndex = Best;
				}


				network.ChoromesToNeurons(OneLevel.Individual, 2);
				network.SaveLearnedData(layersize, file); //Save learned data for test
			}

			else //If NL Wins. Copy toBP
			{
				cout << "\n<------- NSP wins ------>\n";

				//1: GetBestTable for NSP
				//2. Load nsp individual to network (island=1)
				//3. Get individual from network (island=2) into onelevel individual
				//4. Pass onelevel individual to it's species

				//GetBestTable for NSP
				NeuronLevel.GetBestTable(NeuronLevel.NoSpecies - 1);
				NeuronLevel.Join();

				//Load nsp individual to network (island=1)
				network.ChoromesToNeurons(NeuronLevel.Individual, 1);

				//Get individual from network (island=2) into onelevel individual
				OneLevel.Individual = network.Neurons_to_chromes(2);


				//Pass individual back into species

				//Join
				int index = 0;

				for (int row = 0; row < OneLevel.NoSpecies; row++)
				{
					for (int col = 0; col < OneLevel.SpeciesSize[row]; col++)
					{
						OneLevel.TableSp[row].SingleSp[col] = OneLevel.Individual[index];
						index++;
					}

				}


				int Best = 0;


				for (int s = 0; s < OneLevel.SpeciesSize[0]; s++)
					OneLevel.Species[0].Population[Best].Chrome[s] = OneLevel.TableSp[0].SingleSp[s];


				network.ChoromesToNeurons(NeuronLevel.Individual, 1);
				network.SaveLearnedData(layersize, file);


			}

			TotalEval = NeuronLevel.TotalEval + total_epoch; //Add total evals of both islands
			Train = network.TestTrainingData(layersize, trainfile, trainsize, file, input, output);

			cout << "< -- Train %: " << Train << " Total Evals: " << TotalEval << " Hidden: " << hidden << " -- >" << endl;

			if (Train >= mintrain)
			{
				Test = network.TestLearnedData(layersize, testfile, testsize, file, input, output);

				Cycles = Error;
				Error = Test;
				Sucess = true;
				break;
			}

		}	//end while


		Test = network.TestLearnedData(layersize, testfile, testsize, file, input, output);
		Error = Test;

		Test = network.TestLearnedData(layersize, testfile, testsize, file, input, output);
		cout << Test << " was the test in general" << endl;
		out1 << endl;
		out1 << Test << " was the test in general" << endl;
		out2 << h << "   " << Error << "   " << TotalEval << "  " << Train << "  " << Test << endl;

	}
}

int main(void)
{

	int VSize = 90;

	ofstream out1;
	out1.open("Oneout1.txt");
	ofstream out2;
	out2.open("Oneout2.txt"); //Actual vs predicted values for each run
	ofstream out3;
	out3.open("Oneout3.txt"); //Summary of each experimental run
	ofstream out4;
	out4.open("Oneout4.txt"); //Summary of each hidden neuron x number of experimental runs


	ofstream console_output;
	console_output.open("Console_Output.txt");

	FILE * pFile;
	pFile = fopen("Output3all.txt", "a");

	for (int hidden = 4; hidden < 13; hidden += 2) //Number of hidden neurons to use. -> 4, 6, 8, 10, 12
	{
		Sizes EvalAverage;
		Layer ErrorAverage;
		Layer CycleAverage;

		//Initialize all to zeros
		int MeanEval = 0;
		double MeanError = 0;
		double MeanCycle = 0;

		int EvalSum = 0;
		double ErrorSum = 0;
		double CycleSum = 0;
		double maxrun = 20; //Number of experimental runs

		int success = 0;

		for (int run = 1; run <= maxrun; run++)
		{
			CombinedEvolution Combined;

			//Run the main CC algorithm
			Combined.Procedure(false, hidden, out1, out2, out3, 0, 0);

			if (Combined.GetSucess())
			{
				success++;

				ErrorAverage.push_back(Combined.GetError());
				MeanError += Combined.GetError();

				CycleAverage.push_back(Combined.GetCycle());
				MeanCycle += Combined.GetCycle();
			}

			EvalAverage.push_back(Combined.GetEval());
			MeanEval += Combined.GetEval();

		}

		MeanEval = MeanEval / EvalAverage.size();
		MeanError = MeanError / ErrorAverage.size();
		MeanCycle = MeanCycle / CycleAverage.size();

		for (int a = 0; a < EvalAverage.size(); a++)
			EvalSum += (EvalAverage[a] - MeanEval)*(EvalAverage[a] - MeanEval);

		EvalSum = EvalSum / EvalAverage.size();
		EvalSum = sqrt(EvalSum);

		EvalSum = 1.96*(EvalSum / sqrt(EvalAverage.size()));
		for (int a = 0; a < CycleAverage.size(); a++)
			CycleSum += (CycleAverage[a] - MeanCycle)*(CycleAverage[a] - MeanCycle);

		CycleSum = CycleSum / CycleAverage.size();
		CycleSum = sqrt(CycleSum);
		CycleSum = 1.96*(CycleSum / sqrt(CycleAverage.size()));

		//Confidence Interval
		for (int a = 0; a < ErrorAverage.size(); a++)
			ErrorSum += (ErrorAverage[a] - MeanError)*(ErrorAverage[a] - MeanError);

		ErrorSum = ErrorSum / ErrorAverage.size();
		ErrorSum = sqrt(ErrorSum);
		ErrorSum = 1.96*(ErrorSum / sqrt(ErrorAverage.size()));
	
		out3 << hidden << "  " << MeanEval << "  " << EvalSum << "  " << MeanError << "  " << ErrorSum << " " << MeanCycle << "  " << CycleSum << "  " << success << "  " << endl;
		out4 << hidden << "  " << MeanEval << " " << success << "  " << endl;

		EvalAverage.empty();
		ErrorAverage.empty();
		CycleAverage.empty();
	}

	out3 << "\\hline" << endl;

	fprintf(pFile, " \n -----------------  \n");

	fclose(pFile);
	out1.close();
	out2.close();
	out3.close();
	out4.close();
	console_output.close();

	return 0;
};




